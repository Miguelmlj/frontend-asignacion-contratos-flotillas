export const removeVariables = () => {
    window.sessionStorage.removeItem('empresa')
    window.sessionStorage.removeItem('sucursal')
    window.sessionStorage.removeItem('asig_contratos')
    window.sessionStorage.removeItem('clientes_flotillas')
    window.sessionStorage.removeItem('responsable') 
}