import React,{useState, useEffect} from 'react'
import '../../css/modales/ModalClientes.css'

export const ModalClientes = ({ data, editMode, onSubmit}) => {
    const [cliente, setCliente] = useState({})
    const Editar = 'Editar'
    const Registrar = 'Registrar'

    useEffect(() => {
        setCliente(data)
    }, [data])
    
    const onChange = (e) => {
        setCliente({
            ...cliente,
            [e.target.name]: e.target.value
        })
    }

    const resetFields = () => {
        setCliente({
            RFC:"",
            Razon_social:"",
            Nombre_corto:"",
            Num_cliente:"",
            Ubicacion:""  
        })
    }

  return (
    <div className='bg-white-modal'>
        <h5 className='text-center text-dark'>Clientes</h5>
        <div className='row'>
            <div className='container col-12 mt-3'>
                <form>
                    <label 
                    className="bg-secondary input-group-text text-light font-weight-normal"
                    >
                        Número Cliente
                    </label>
                    <input 
                    type="number" 
                    className="form-control mb-2" 
                    tabIndex={1} 
                    value={cliente.Num_cliente} 
                    onChange={onChange} 
                    name="Num_cliente" 
                    autoComplete='off' 
                    />
                    
                    <label 
                    className="bg-secondary input-group-text text-light font-weight-normal"
                    >
                        RFC
                    </label>
                    <input 
                    type="text" 
                    className="form-control mb-2" 
                    tabIndex={2} 
                    value={cliente.RFC} 
                    onChange={onChange} 
                    name="RFC" 
                    autoComplete='off' 
                    />
                    
                    <label 
                    className="bg-secondary input-group-text text-light font-weight-normal"
                    >
                        Razón Social
                    </label>
                    <input 
                    type="text" 
                    className="form-control mb-2" 
                    tabIndex={3} 
                    value={cliente.Razon_social} 
                    onChange={onChange} 
                    name="Razon_social" 
                    autoComplete='off' 
                    />
                    
                    <label 
                    className="bg-secondary input-group-text text-light font-weight-normal"
                    >
                        Nombre Corto
                    </label>
                    <input 
                    type="text" 
                    className="form-control mb-2" 
                    tabIndex={4} 
                    value={cliente.Nombre_corto} 
                    onChange={onChange} 
                    name="Nombre_corto" 
                    autoComplete='off' 
                    />
                    
                    <label 
                    className="bg-secondary input-group-text text-light font-weight-normal"
                    >
                        Ubicación
                    </label>
                    <input 
                    type="text" 
                    className="form-control mb-4" 
                    tabIndex={5} 
                    value={cliente.Ubicacion} 
                    onChange={onChange} 
                    name="Ubicacion" 
                    autoComplete='off' 
                    />


                </form> 
            </div>           
        </div>
        <div className='row justify-content-center'>
            <div className='col-auto'>
                <button
                className='btn btn-info text-light font-italic'
                type='button'
                tabIndex={6}
                onClick={
                    () => {
                        onSubmit(cliente)
                        resetFields()
                    }
                }
                >
                    {editMode ? Editar : Registrar}

                </button>
            </div>
        </div>
    </div>
  )
}
