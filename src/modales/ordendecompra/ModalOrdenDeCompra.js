import React,{ useState, useEffect, useRef } from 'react'

import '../../css/modales/ModalClientes.css'
import '../../css/modales/ModalOrdenCompra.css'
import { axiosPostService } from '../../services/asignacionLoteService/AsignacionLoteService'
import { ApiUrl } from '../../services/ApiRest'
import axios from 'axios'
export const ModalOrdenDeCompra = ({ data, editMode, onSubmit, tipoVehiculos, nombreDeClientes }) => {
    const [ordenDeCompra, setOrdenDeCompra] = useState({})
    const Editar = "Editar"
    const Registrar = "Registrar"
    const ref = useRef();

    useEffect(() => {
        if ( !editMode ) {
            setOrdenDeCompra({
                ...data,
                TipoVehiculo: tipoVehiculos.length > 0 ? tipoVehiculos[0].Descripcion : "",
                Cliente: nombreDeClientes.length > 0 ? nombreDeClientes[0].Nombre_corto : "",
                Ubicacion: nombreDeClientes.length > 0 ? nombreDeClientes[0].Ubicacion : "",
            })
            return;
        }
        setOrdenDeCompra(data)
    }, [data])

    const onChange = async(e) => {
        if ( e.target.name === "DocumentoPDF" ) {
            const file = e.target.files[0];
            setOrdenDeCompra({
                ...ordenDeCompra,
                [e.target.name]: file
            })
            return;
        }
        if ( e.target.name === "Cliente" ) {
            const [ nombre_corto, ubicacion ] = e.target.value.split("|")
            setOrdenDeCompra({
                ...ordenDeCompra,
                Cliente: nombre_corto,
                Ubicacion: ubicacion
            })
            return;
        }
        setOrdenDeCompra({
            ...ordenDeCompra,
            [e.target.name]: e.target.value
        })
    }

    /* const changeSelectTipoVehiculos = (e) => {
        console.log('another function')
    }

    const changeSelectNombreDeClientes = (e) => {
        console.log('whatever');
    } */

    const resetFields = (e) => {
        setOrdenDeCompra({
            Cliente:"",
            Ubicacion:"",
            OrdenCompra:"",
            Cantidad:"",
            TipoVehiculo:"",
            Existencia:"",
            DocumentoPDF:null
        })
        ref.current.value = "";
    }

  return (
    <div className='bg-white-modal'>
        <h5 className='text-center text-dark'>Orden De Compra</h5>
        <div className='row'>
            <div className='container col-12 mt-3'>
                <form>
                    <label 
                    className="bg-secondary input-group-text text-light font-weight-normal"
                    >
                        Cliente
                    </label>
                    <select name='Cliente' className='form-select seleccionable mb-2 select-class-1' onChange={(e) => onChange(e)} disabled={ nombreDeClientes.length === 0 }>
                        {
                            nombreDeClientes.map(obj => {
                                return (
                                    <option selected={ordenDeCompra.Cliente === obj.Nombre_corto} value={`${obj.Nombre_corto}|${obj.Ubicacion}`}>{`${obj.Nombre_corto} ${obj.Ubicacion}`}</option>
                                )
                            })
                        }
                    </select>
                    
                    <label 
                    className="bg-secondary input-group-text text-light font-weight-normal"
                    >
                        Orden de Compra
                    </label>
                    <input 
                    type="text" 
                    className="form-control mb-2" 
                    tabIndex={2} 
                    value={ordenDeCompra.OrdenCompra} 
                    onChange={onChange} 
                    name="OrdenCompra" 
                    autoComplete='off' 
                    />
                    <label 
                    className="bg-secondary input-group-text text-light font-weight-normal"
                    >
                        Cantidad
                    </label>
                    <input 
                    type="number" 
                    className="form-control mb-2" 
                    tabIndex={2} 
                    value={ordenDeCompra.Cantidad} 
                    onChange={onChange} 
                    name="Cantidad" 
                    autoComplete='off' 
                    />
                    
                    <label 
                    className="bg-secondary input-group-text text-light font-weight-normal"
                    >
                        Tipo de Vehiculo
                    </label>
                    <select name='TipoVehiculo'  className='form-select seleccionable mb-2 select-class-1' onChange={(e) => onChange(e)} disabled={ tipoVehiculos.length === 0 }>
                        {
                            tipoVehiculos.map(obj => {
                                return (
                                    <option selected={ordenDeCompra.TipoVehiculo === obj.Descripcion} value={obj.Descripcion}>{obj.Descripcion}</option>
                                )
                            })
                        }
                    </select>
                    
                    <label 
                    className="bg-secondary input-group-text text-light font-weight-normal"
                    >
                        Existencia
                    </label>
                    <input 
                    type="number" 
                    className="form-control mb-2" 
                    tabIndex={4} 
                    value={ordenDeCompra.Existencia} 
                    onChange={onChange} 
                    name="Existencia" 
                    autoComplete='off' 
                    />
                    
                    {/* <label 
                    className="bg-secondary input-group-text text-light font-weight-normal"
                    >
                        DocumentoPDF
                    </label> */}
                    <input 
                    type="file"
                    accept='.pdf' 
                    className='custom-file-upload'
                    ref={ref}
                   /*  className="form-control mb-4" 
                    tabIndex={5} 
                    value={ordenDeCompra.DocumentoPDF} */ 
                    onChange={onChange} 
                    name="DocumentoPDF" 
                    // autoComplete='off' 
                    />

                </form> 
            </div>           
        </div>
        <div className='row justify-content-center'>
            <div className='col-auto'>
                <button
                className='btn btn-info text-light font-italic mt-2'
                type='button'
                tabIndex={6}
                onClick={
                    () => {
                        onSubmit(ordenDeCompra)
                        resetFields()
                    }
                }
                >
                    {editMode ? Editar : Registrar}
                </button>
            </div>
        </div>
    </div>
  )
}
