import React, { useState, useEffect } from 'react'
import { toast } from 'react-toastify'

import { VistaPreviaFolioCompra } from './reactToPrint/VistaPreviaFolioCompra'
import { invertirCadenaFecha } from '../../../helpers/fecha'
import { axiosPostService } from '../../../services/asignacionLoteService/AsignacionLoteService'
import '../../../css/asignacionContratos/asignacionReferencia/asignacionReferencia.css'
import { ApiUrl } from '../../../services/ApiRest'
import { removerComas } from '../../../helpers/formatoMoneda'
import { isADotValue, hasPointTheInputValue, isNumber, getTotalPoints } from '../../../helpers/validarInputs'

const AsignacionFolioCompra = ({ agencia, clientes }) => {
  const [VINClientes, setVINClientes] = useState([])
  const [NombresLoteCliente, setNombresLoteCliente] = useState([])
  const [isActiveButtonExcel, setisActiveButtonExcel] = useState(true)
  const [isActiveButtonActualizar, setisActiveButtonActualizar] = useState(false)
  const [isShowedTable, setIsShowedTable] = useState(false)
  const [inputsObject, setInputsObject] = useState({
    NombreCliente: "",
    NombreLote: "",
    Ubicacion: "",
    numCliente: 0
  })
  let url = '';

  useEffect(() => {
    getNombresLoteCliente( clientes[0] )
  }, [clientes])

  const getNombresLoteCliente = async (cliente) => {
    const { Nombre_corto, Ubicacion, Num_cliente } = cliente;
    setInputsObject({...inputsObject, NombreCliente: Nombre_corto, Ubicacion: Ubicacion, numCliente: Num_cliente})

    url = ApiUrl + "api/foliocompra/getNombresLotesCliente"
    const body = { agencia: agencia, Nombre_corto: Nombre_corto, Ubicacion: Ubicacion, numCliente: Num_cliente }
    const nombres_lote_cliente = await axiosPostService( url, body );
    
    if ( nombres_lote_cliente.length === 0 ) {
      resetValuesVINClientes()
      return;
    }

    const { Folio_lote, NumCliente } = nombres_lote_cliente[0];
    setNombresLoteCliente([]);
    setNombresLoteCliente( nombres_lote_cliente );
    getLoteCliente( Folio_lote, Nombre_corto, NumCliente );

  }

  const getLoteCliente = async (Folio_lote, Nombre_corto, NumCliente) => {
    url = ApiUrl + "api/foliocompra/getLoteCliente"
    const body_lote = { agencia: agencia, Folio_lote: Folio_lote, NumCliente: NumCliente };
    const vins_lote = await axiosPostService( url, body_lote );
    setVINClientes( vins_lote )

  }

  const resetValuesVINClientes = () => {
    setVINClientes([]);
    setNombresLoteCliente([])
    // toast("No se han encontrado lotes para asignación de referencias.");
  } 
  
  const changeSelectClientes = (e) => {
    if ( isActiveButtonActualizar ) setisActiveButtonActualizar(false)
    if ( !isActiveButtonExcel ) setisActiveButtonExcel(true)
    setIsShowedTable(false)
    const split = e.target.value.split("|")
    const [ Ubicacion, Nombre_cliente, Num_cliente ] = split;
    setInputsObject({...inputsObject, NombreCliente: Nombre_cliente, Ubicacion: Ubicacion, numCliente: Num_cliente})
    const objetoCliente = { Nombre_corto: Nombre_cliente, Ubicacion: Ubicacion, Num_cliente: Num_cliente }
    getNombresLoteCliente( objetoCliente )
  }

  const changeSelectNombresLoteCliente = (e) => {
    if ( isActiveButtonActualizar ) setisActiveButtonActualizar(false)
    if ( !isActiveButtonExcel ) setisActiveButtonExcel(true)
    setIsShowedTable( false );
    const Folio_lote = e.target.value;
    const Nombre_cliente = inputsObject.NombreCliente;
    const Num_Cliente = inputsObject.numCliente;
    getLoteCliente( Folio_lote, Nombre_cliente, Num_Cliente)
  }

  const onGenerateTable = () => {
    setIsShowedTable(!isShowedTable)
  }

  const validateInput = (e, vinRegistro, valorDelCampoEnArreglo) => {
    if (isNumber(e.target.value)) {
      calcularValores(e.target.name, vinRegistro, e.target.value)
      return;
    }
    
    if (!isNumber(e.target.value)) {
      if (isADotValue(e.target.value)) {
          (!hasPointTheInputValue( valorDelCampoEnArreglo ))
          ? calcularValores(e.target.name, vinRegistro, e.target.value)
          : (getTotalPoints(e.target.value) !== 2) && calcularValores(e.target.name, vinRegistro, valorDelCampoEnArreglo.substring(0, valorDelCampoEnArreglo.toString().length - 1))
          
      }
    }

  }

  const calcularValores = (propiedad, vinRegistro, valor) => {
    // console.log('funcion calcular valores', `propiedad: ${propiedad} valor: ${valor}`);
    //removerComas.
    
      const updateListaClientes = VINClientes.map((row) => {
        if ( row.VIN === vinRegistro ) {
          let updaterow;
          if ( propiedad === "Monto_plan_piso" ) {
            updaterow = {
              ...row,
              [propiedad] : valor
            }
          }
          if ( propiedad === "Monto_deposito_cuenta_cheques" ) {
            updaterow = {
              ...row,
              [propiedad] : valor
            }
          }
          
          if ( propiedad === "Monto_total" ) {
            updaterow = {
              ...row,
              [propiedad] : valor
            }
          }
          if ( propiedad === "Inversion_inicial" ) {
            updaterow = {
              ...row,
              [propiedad] : valor
            }
          }
          /* else {
            updaterow = {
              ...row,
              [e.target.name]: e.target.value
            }
          } */
          
          return updaterow
        }
        return row
      })
      setVINClientes(updateListaClientes)
     
    

  }

  const onChange = (e, vinRegistro, valorDelCampoEnArreglo) => {
    if ( !isActiveButtonActualizar ) setisActiveButtonActualizar(true)
    if ( isActiveButtonExcel ) setisActiveButtonExcel(false)

    if ( e.target.name === "Monto_plan_piso" ) {
      validateInput(e, vinRegistro, valorDelCampoEnArreglo)
      return;
    }
    
    if ( e.target.name === "Monto_deposito_cuenta_cheques" ) {
      validateInput(e, vinRegistro, valorDelCampoEnArreglo)
      return;
    }
    
    if ( e.target.name === "Monto_total" ) {
      validateInput(e, vinRegistro, valorDelCampoEnArreglo)
      return;
    }

    if ( e.target.name === "Inversion_inicial" ) {
      validateInput(e, vinRegistro, valorDelCampoEnArreglo)
      return;
    }

    //Inversion_inicial

    const updateListaClientes = VINClientes.map((row) => {
      if ( row.VIN === vinRegistro ) {
        let updaterow;
        if ( e.target.name === "Tasa_porcentaje_enganche" ) {
          updaterow = {
            ...row,
            [e.target.name]       : e.target.value,
            ['Monto_total']       : new Intl.NumberFormat('es-MX').format(row.Precio_factura * (1 - (e.target.value / 100))),
            ['Inversion_inicial'] : new Intl.NumberFormat('es-MX').format(row.Precio_factura * (e.target.value / 100) ),
          }
        }
        else {
          updaterow = {
            ...row,
            [e.target.name]: e.target.value
          }
        }
        
        return updaterow
      }
      return row
    })

    setVINClientes(updateListaClientes)
  }

  const handleStateButtonUpdateLote = () => {
    if ( isActiveButtonActualizar ) setisActiveButtonActualizar(false)
    if ( !isActiveButtonExcel ) setisActiveButtonExcel(true);
    // setIsShowedTable(false);
    /* const cliente = getObjectClienteSelected();
    getNombresLoteCliente(cliente) */
  }

  const getObjectClienteSelected = () => {
    let numero_cliente = inputsObject.numCliente;
    let cliente = clientes.find((obj) => obj.Num_cliente == numero_cliente)
    return cliente;
  }

  const onBlur = (e, vinRegistro) => {
    if ( e.target.name === "Monto_plan_piso" ) {
      updateArregloClientes(e, vinRegistro, e.type)
      return;
    }
    if ( e.target.name === "Monto_deposito_cuenta_cheques" ) {
      updateArregloClientes(e, vinRegistro, e.type)
      return;
    }
    if ( e.target.name === "Monto_total" ) {
      updateArregloClientes(e, vinRegistro, e.type)
      return;
    }
    if ( e.target.name === "Inversion_inicial" ) {
      updateArregloClientes(e, vinRegistro, e.type)
      return;
    }
  }

  const onFocus = (e, vinRegistro) => {
    if ( e.target.name === "Monto_plan_piso" ) {
      updateArregloClientes(e, vinRegistro, e.type)
      return;
    }
    if ( e.target.name === "Monto_deposito_cuenta_cheques" ) {
      updateArregloClientes(e, vinRegistro, e.type)
      return;
    }
    if ( e.target.name === "Monto_total" ) {
      updateArregloClientes(e, vinRegistro, e.type)
      return;
    }
    if ( e.target.name === "Inversion_inicial" ) {
      updateArregloClientes(e, vinRegistro, e.type)
      return;
    }

  }

  const updateArregloClientes = ( e, vinRegistro, tipo ) => {
    const updateListaClientes = VINClientes.map((row) => {
      if ( row.VIN === vinRegistro ) {
        let updaterow;
        if ( e.target.name === "Monto_plan_piso" ) {
          updaterow = {
            ...row,
            [e.target.name] : tipo === "blur" ? new Intl.NumberFormat('es-MX').format(e.target.value) : removerComas(e.target.value)
          }
        }
        if ( e.target.name === "Monto_deposito_cuenta_cheques" ) {
          
          updaterow = {
            ...row,
            [e.target.name] : tipo === "blur" ? new Intl.NumberFormat('es-MX').format(e.target.value) : removerComas(e.target.value)
          }
        }
        if ( e.target.name === "Monto_total" ) {
          
          updaterow = {
            ...row,
            [e.target.name] : tipo === "blur" ? new Intl.NumberFormat('es-MX').format(e.target.value) : removerComas(e.target.value)
          }
        }
        if ( e.target.name === "Inversion_inicial" ) {
          
          updaterow = {
            ...row,
            [e.target.name] : tipo === "blur" ? new Intl.NumberFormat('es-MX').format(e.target.value) : removerComas(e.target.value)
          }
        }
        
        return updaterow
      }
      return row
    })
    setVINClientes(updateListaClientes)
  }

  return (
    <>
      <div className="row m-2">
        <div className="col-6">
          <div className="row d-flex justify-content-between pl-2 pr-4">
            <h6 className='mr-4'>1.- Seleccionar Cliente: </h6>
            <select className='form-select select-class-1' onChange={(e) => changeSelectClientes(e)}>
                {clientes.map(cliente => {
                  return (
                    <option value={`${cliente.Ubicacion}|${cliente.Nombre_corto}|${cliente.Num_cliente}`} selected={cliente.Id === 1}>{`${cliente.Nombre_corto} ${cliente.Ubicacion}`}</option>
                  )
                })}
            </select>
          </div>
        </div>
      </div>
      <div className="row m-2 bottom-space">
          <div className="col-6">
            <div className="row d-flex justify-content-between pl-2 pr-4">
                <h6 className='mr-4'>2.- Nombre de Lote: </h6>
                <select className='form-select select-class-1' onChange={(e) => changeSelectNombresLoteCliente(e)} disabled={ NombresLoteCliente.length === 0 }>
                { 
                    NombresLoteCliente.map(objeto => {
                      return (
                        <option value={objeto.Folio_lote} selected={NombresLoteCliente[0].Nombre_lote == objeto.Nombre_lote}>{ `${objeto.Nombre_lote} ${invertirCadenaFecha(objeto.Fecha_elaboracion.substring(0, 10))}` }</option>
                      )
                    })
                 }
                </select>
            </div>
          </div>
      </div>
      <div className='row m-2'>
         <h6>3.- Asignar Folio de Compra</h6>
      </div>
      {
        !isShowedTable &&
        <div className='row table-responsive pl-2'>
          <table className='table display compact' style={{width:'150%'}}>
            <thead style={{backgroundColor:'#1565C0', color:'white'}}>
              <tr className='text-center'>
                <th style={{width:'5%'}}>Cliente</th>     
                <th style={{width:'5%'}}>Marca</th>     
                <th style={{width:'5%'}}>Unidad</th>     
                <th style={{width:'5%'}}>VIN</th>     
                <th style={{width:'5%'}}>No. Factura</th>     
                <th style={{width:'5%'}}>Precio Factura</th>     
                <th style={{width:'5%'}}>Orden Compra</th>     
                <th style={{width:'5%'}}>Referencia</th>

                <th style={{width:'5%'}}>Folio Compra Contrato</th>
                <th style={{width:'5%'}}>Fecha Compra Contrato</th>
                <th style={{width:'5%'}}>Monto Plan Piso</th>
                <th style={{width:'5%'}}>Monto Depósito Cuenta Cheques</th>
                <th style={{width:'5%'}}>Tasa</th>
                <th style={{width:'5%'}}>Monto Total</th>     
                <th style={{width:'5%'}}>Inversión Inicial</th>     
              </tr>
            </thead>
            <tbody style={{backgroundColor:'#FFFFE0'}}>
              {
                VINClientes.length > 0 ?
                VINClientes.map((registro) => {
                  return (
                    <tr className='text-center'>
                      <td style={{width:'5%'}}>{registro.Nombre_cliente}</td>
                      <td style={{width:'5%'}}>{registro.Marca}</td>
                      <td style={{width:'5%'}}>{registro.Unidad}</td>
                      <td style={{width:'5%'}}>{registro.VIN}</td>
                      <td style={{width:'5%'}}>{registro.Numero_factura}</td>
                      <td style={{width:'5%'}}>{new Intl.NumberFormat('es-MX').format(registro.Precio_factura)}</td>
                      <td style={{width:'5%'}}>{registro.Orden_compra}</td>
                      <td style={{width:'5%'}}>{registro.Referencia}</td>

                      <td style={{width:'5%'}}>
                        <input type="text" className="form-control" onChange={(e) => onChange(e, registro.VIN, registro.Folio_compra_contrato)} value={registro.Folio_compra_contrato} name="Folio_compra_contrato" autoComplete='off'/>
                      </td>
                      <td style={{width:'5%'}}>
                        <input type="date" className="form-control" min="2018-01-01" onChange={(e) => onChange(e, registro.VIN, registro.Fecha_compra_contrato)} value={registro.Fecha_compra_contrato.substring(0,10)} name="Fecha_compra_contrato" autoComplete='off'/>
                      </td>
                      <td style={{width:'5%'}}>
                        <input 
                        type="text" 
                        className="form-control"
                        onFocus={(e) => onFocus(e, registro.VIN)}
                        onBlur={(e) => onBlur(e, registro.VIN)}
                        onChange={(e) => onChange(e, registro.VIN, registro.Monto_plan_piso)} 
                        value={registro.Monto_plan_piso} 
                        name="Monto_plan_piso" 
                        autoComplete='off'
                        />
                      </td>
                      <td style={{width:'5%'}}>
                        <input 
                        type="text" 
                        className="form-control"
                        onFocus={(e) => onFocus(e, registro.VIN)}
                        onBlur={(e) => onBlur(e, registro.VIN)} 
                        onChange={(e) => onChange(e, registro.VIN, registro.Monto_deposito_cuenta_cheques)} 
                        value={registro.Monto_deposito_cuenta_cheques} 
                        name="Monto_deposito_cuenta_cheques" 
                        autoComplete='off'
                        />
                      </td>
                      <td style={{width:'5%'}}>
                        <input type="number" className="form-control" onChange={(e) => onChange(e, registro.VIN, registro.Tasa_porcentaje_enganche)} value={registro.Tasa_porcentaje_enganche} name="Tasa_porcentaje_enganche" autoComplete='off'/>
                      </td>
                      <td style={{width:'5%'}}>
                        <input 
                        type="text" 
                        className="form-control"
                        onFocus={(e) => onFocus(e, registro.VIN)}
                        onBlur={(e) => onBlur(e, registro.VIN)} 
                        onChange={(e) => onChange(e, registro.VIN, registro.Monto_total)} 
                        value={registro.Monto_total} 
                        name="Monto_total" 
                        autoComplete='off'
                        />
                      </td>
                      <td style={{width:'5%'}}>
                        <input 
                        type="text" 
                        className="form-control"
                        onFocus={(e) => onFocus(e, registro.VIN)} 
                        onBlur={(e) => onBlur(e, registro.VIN)} 
                        onChange={(e) => onChange(e, registro.VIN, registro.Inversion_inicial)} 
                        value={registro.Inversion_inicial} 
                        name="Inversion_inicial" 
                        autoComplete='off'
                        />
                      </td>
                    </tr>
                  )
                })
                :
                <tr className='p-2'>
                  <strong>No existen lotes creados del cliente seleccionado</strong>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>

              }
            </tbody>
          </table>
        </div>
      }
      <VistaPreviaFolioCompra
      agencia={ agencia }
      data={ VINClientes }
      isShowedTable={isShowedTable}
      handleStateButtonUpdateLote={handleStateButtonUpdateLote}
      isActiveButtonExcel={isActiveButtonExcel}
      isActiveButtonActualizar={isActiveButtonActualizar}
      />
      <div className='row m-2'>
        <button
          type='button'
          className='btn btn-info mt-2 mb-2'
          onClick={onGenerateTable}
          disabled={VINClientes.length === 0}
        >
          { isShowedTable ? "Editar" : "Vista Previa"}
        </button>
      </div>
    </>
  )
}

export default AsignacionFolioCompra