import React, { useState, useEffect } from 'react'

import { ResumenDeContratosTable } from './reactToPrint/ResumenDeContratosTable'
import { invertirCadenaFecha } from '../../../helpers/fecha'
import { axiosPostService } from '../../../services/asignacionLoteService/AsignacionLoteService'
import '../../../css/asignacionContratos/asignacionReferencia/asignacionReferencia.css'
import { ApiUrl } from '../../../services/ApiRest'

const ResumenDeContratos = ({ agencia, clientes }) => {
    const [VINClientes, setVINClientes] = useState([])
    const [NombresLoteCliente, setNombresLoteCliente] = useState([])
    const [isShowedTable, setIsShowedTable] = useState(false)
    const [inputsObject, setInputsObject] = useState({
      NombreCliente: "",
      NombreLote: "",
      Ubicacion: "",
      numCliente: 0
    })
    let url = '';

    useEffect(() => {
      getNombresLoteCliente( clientes[0] )
    }, [clientes])

    const getNombresLoteCliente = async( cliente ) => {
      const { Nombre_corto, Ubicacion, Num_cliente } = cliente;
      setInputsObject({...inputsObject, NombreCliente: Nombre_corto, Ubicacion: Ubicacion, numCliente: Num_cliente})

      url = ApiUrl + "api/resumen/getNombresLotesCliente"
      const body = { agencia: agencia, Nombre_corto: Nombre_corto, Ubicacion: Ubicacion, numCliente: Num_cliente }
      const nombres_lote_cliente = await axiosPostService( url, body );

      if ( nombres_lote_cliente.length === 0 ) {
        resetValuesVINClientes();
        return;
      }

      const { Folio_lote, NumCliente } = nombres_lote_cliente[0];
      setNombresLoteCliente([]);
      setNombresLoteCliente( nombres_lote_cliente );
      getLoteCliente( Folio_lote, Nombre_corto, NumCliente );
    }

    const getLoteCliente = async ( Folio_lote, Nombre_corto, NumCliente ) => {
      url = ApiUrl + "api/resumen/getLoteCliente";
      const body_lote = { agencia: agencia, Folio_lote: Folio_lote, NumCliente: NumCliente};
      const vins_lote = await axiosPostService( url, body_lote );
      setVINClientes( vins_lote )
    }

    const resetValuesVINClientes = () => {
      setVINClientes([]);
      setNombresLoteCliente([])
    }
    
    const changeSelectClientes = (e) => {
      const split = e.target.value.split("|")
      const [ Ubicacion, Nombre_cliente, Num_cliente ] = split;
      setInputsObject({...inputsObject, NombreCliente: Nombre_cliente, Ubicacion: Ubicacion, numCliente: Num_cliente})
      const objetoCliente = { Nombre_corto: Nombre_cliente, Ubicacion: Ubicacion, Num_cliente: Num_cliente }
      getNombresLoteCliente( objetoCliente )
    }

    const changeSelectNombresLoteCliente = (e) => {
      const Folio_lote = e.target.value;
      const Nombre_cliente = inputsObject.NombreCliente;
      const Num_Cliente = inputsObject.numCliente;
      getLoteCliente( Folio_lote, Nombre_cliente, Num_Cliente )
    }

    return (
    <>
        <div className="row m-2">
            <div className="col-6">
              <div className="row d-flex justify-content-between pl-2 pr-4">
                <h6 className='mr-4'>Seleccionar Cliente: </h6>
                <select className='form-select select-class-1' onChange={(e) => changeSelectClientes(e)}>
                    {clientes.map(cliente => {
                      return (
                        <option value={`${cliente.Ubicacion}|${cliente.Nombre_corto}|${cliente.Num_cliente}`} selected={cliente.Id === 1}>{`${cliente.Nombre_corto} ${cliente.Ubicacion}`}</option>
                      )
                    })}
                </select>
              </div>
            </div>
        </div>
        <div className="row m-2 bottom-space">
          <div className="col-6">
            <div className="row d-flex justify-content-between pl-2 pr-4">
                <h6 className='mr-4'>Nombre de Lote: </h6>
                <select className='form-select select-class-1' onChange={(e) => changeSelectNombresLoteCliente(e)} disabled={NombresLoteCliente.length === 0}>
                { 
                    NombresLoteCliente.map(objeto => {
                      return (
                        <option value={objeto.Folio_lote} selected={NombresLoteCliente[0].Nombre_lote == objeto.Nombre_lote}>{ `${objeto.Nombre_lote} ${invertirCadenaFecha(objeto.Fecha_elaboracion.substring(0, 10))}` }</option>
                      )
                    })
                 }
                </select>
            </div>
          </div>
        </div>
        <div className='row m-2'>
           <h6>Información Lote:</h6>
        </div>
        <ResumenDeContratosTable
        data={VINClientes}
        />
    </>
    )
}

export default ResumenDeContratos