import React, { useRef, Component, useEffect, useState } from "react"
import * as XLSX from "xlsx"
import { toast } from "react-toastify"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileExcel } from '@fortawesome/free-solid-svg-icons';
import { axiosPatchService } from '../../../../services/asignacionLoteService/AsignacionLoteService';
import { ApiUrl } from '../../../../services/ApiRest';
import { invertirCadenaFecha, FechaDeHoyYYMMDD } from "../../../../helpers/fecha";
import { removerComas } from "../../../../helpers/formatoMoneda";

class TablaFolioCompra extends Component {
    render() {
        const data = this.props.data;
        return (
            <table className='table display compact' style={{width:'150%'}}>
                <thead style={{backgroundColor:'#1565C0', color:'white'}}>
                    <tr className="text-center">
                        <th style={{width:'5%'}}>Cliente</th>     
                        <th style={{width:'5%'}}>Marca</th>     
                        <th style={{width:'5%'}}>Unidad</th>     
                        <th style={{width:'5%'}}>VIN</th>     
                        <th style={{width:'5%'}}>No. Factura</th>     
                        <th style={{width:'5%'}}>Precio Factura</th>     
                        <th style={{width:'5%'}}>Orden Compra</th>     
                        <th style={{width:'5%'}}>Referencia</th>

                        <th style={{width:'5%'}}>Folio Compra Contrato</th>
                        <th style={{width:'5%'}}>Fecha Compra Contrato</th>
                        <th style={{width:'5%'}}>Monto Plan Piso</th>
                        <th style={{width:'5%'}}>Monto Depósito Cuenta Cheques</th>
                        <th style={{width:'5%'}}>Tasa</th>
                        <th style={{width:'5%'}}>Monto Total</th>
                        <th style={{width:'5%'}}>Inversion Inicial</th>
                    </tr>
                </thead>
                <tbody style={{backgroundColor:'#FFFFE0'}}>
                {
                    data.length > 0 ?
                        data.map((registro) => {
                            return (
                                <tr className='text-center'>
                                  <td>{registro.Nombre_cliente}</td>
                                  <td>{registro.Marca}</td>
                                  <td>{registro.Unidad}</td>
                                  <td>{registro.VIN}</td>
                                  <td>{registro.Numero_factura}</td>
                                  <td>{new Intl.NumberFormat('es-MX').format(registro.Precio_factura)}</td>
                                  <td>{registro.Orden_compra}</td>
                                  <td>{registro.Referencia}</td>

                                  <td>{registro.Folio_compra_contrato}</td>
                                  <td>{invertirCadenaFecha(registro.Fecha_compra_contrato.substring(0,10))}</td>
                                  <td>{registro.Monto_plan_piso}</td>
                                  <td>{registro.Monto_deposito_cuenta_cheques}</td>
                                  <td>{registro.Tasa_porcentaje_enganche}</td>
                                  <td>{registro.Monto_total}</td>
                                  <td>{registro.Inversion_inicial}</td>
                                </tr>
                            )
                        })
                        :
                        ""
                }
                </tbody>
            </table>
        )
    }
}

export const VistaPreviaFolioCompra = ({
    agencia, 
    data, 
    isShowedTable, 
    handleStateButtonUpdateLote,
    isActiveButtonActualizar,
    isActiveButtonExcel }) => {

    const [isActiveButtonUpdate, setIsActiveButtonUpdate] = useState(true)
    const tableRef = useRef();
    let url = '';
    const YYMMDD = FechaDeHoyYYMMDD()

    useEffect(() => {
      if ( isShowedTable ) {
        const isFolioCompraEmpty = data.find(row => (row.Folio_compra_contrato === "" && row.Fecha_compra_contrato.substring(0,10) !== YYMMDD));
        if ( isFolioCompraEmpty !== undefined ){
            if ( isActiveButtonUpdate ) setIsActiveButtonUpdate(false)
            toast("El campo Folio Compra Contrato se encuentra vacío")
            return;
        }
        if (!isActiveButtonUpdate) setIsActiveButtonUpdate(true)
      }
      
    }, [isShowedTable])
    

    const onUpdateLote = async() => {
        url = ApiUrl + "api/foliocompra/updatelotecliente"
        const body = { agencia: agencia, lote: data }
        const updateLotePromise = await axiosPatchService( url, body )
        if ( updateLotePromise.isUpdated ) {
            toast("El lote ha sido actualizado con éxito.");
            handleStateButtonUpdateLote();
            // onExportToExcel();
        }
    }

    const dataExcelFile = () => {
        const selectFieldsdata = data.map((row) => {
            let newRow = {
                "Cliente": row.Nombre_cliente,
                "Marca": row.Marca,
                "Unidad": row.Unidad,
                "Serie": row.VIN,
                "No. Factura": row.Numero_factura,
                "Precio Factura": row.Precio_factura,
                "Orden Compra": row.Orden_compra,
                "Referencia": row.Referencia,
                "Folio compra contrato": row.Folio_compra_contrato,
                "Fecha compra contrato": invertirCadenaFecha(row.Fecha_compra_contrato),
                "Monto plan piso": removerComas(row.Monto_plan_piso),
                "Monto deposito cuenta cheques": removerComas(row.Monto_deposito_cuenta_cheques),
                "Tasa": row.Tasa_porcentaje_enganche,
                "Monto total": removerComas(row.Monto_total),
                "Inversion inicial": removerComas(row.Inversion_inicial)
            }
            return newRow;
        })

        return selectFieldsdata;
    }

    const onExportToExcel = () => {
        const dataExcel = dataExcelFile();
        const Header = ["Cliente", "Marca", "Unidad", "Serie", "No. Factura", "Precio Factura", "Orden Compra", "Referencia", "Folio compra contrato", "Fecha compra contrato", "Monto plan piso", "Monto deposito cuenta cheques", "Tasa", "Monto total", "Inversion inicial"]
        const fileName = "Asignación Contrato";
        const fileNameExtension = "Asignacion_Contrato.xlsx";
        let wb = XLSX.utils.book_new()
        let ws = XLSX.utils.json_to_sheet([])
        XLSX.utils.sheet_add_json(ws, dataExcel, {
            header: Header,
            skipHeader: false,
        })

        XLSX.utils.book_append_sheet(wb, ws, fileName);
        XLSX.writeFile(wb, fileNameExtension)
    }

    return (
        isShowedTable &&
        <div>
            <div className='row pl-2 table-responsive'>
                <TablaFolioCompra data={data} ref={tableRef}/>
            </div>
            <button
            type="button"
            className="btn btn-info mt-2 mb-2 ml-2"
            onClick={onUpdateLote}
            disabled={!isActiveButtonUpdate || !isActiveButtonActualizar}
            >
                Actualizar
            </button>
            <button
            title="Exportar a Excel"
            type="button"
            className="btn btn-outline-success mt-2 mb-2 ml-2"
            onClick={onExportToExcel}
            disabled={!isActiveButtonExcel}
            >
                <FontAwesomeIcon icon={faFileExcel} />
                <small className="ml-2">Excel</small>
            </button>
        </div>
    )
        
}