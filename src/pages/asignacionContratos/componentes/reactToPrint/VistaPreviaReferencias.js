import React, { useRef, Component, useState } from "react"

import * as XLSX from "xlsx"
import { toast } from "react-toastify"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileExcel } from '@fortawesome/free-solid-svg-icons';
import { axiosDeleteServide, axiosPostService, axiosPatchService } from '../../../../services/asignacionLoteService/AsignacionLoteService';
import { ApiUrl } from '../../../../services/ApiRest';

class TablaReferencias extends Component {
    render() {
        const data = this.props.data;
        return (
            <table className='table display compact'>
                <thead style={{backgroundColor:'#1565C0', color:'white'}}>
                    <tr className="text-center">
                        <th>Cliente</th>     
                        <th>Marca</th>     
                        <th>Unidad</th>     
                        <th>VIN</th>     
                        <th>No. Factura</th>     
                        <th>Precio Factura</th>     
                        <th>Orden Compra</th>     
                        <th>Referencia</th>  
                    </tr> 
                </thead>
                <tbody style={{backgroundColor:'#FFFFE0'}}>
                {
                    data.length > 0 ?
                        data.map((registro) => {
                            return (
                                <tr className='text-center'>
                                    <td>{registro.Nombre_cliente}</td>
                                    <td>{registro.Marca}</td>
                                    <td>{registro.Unidad}</td>
                                    <td>{registro.VIN}</td>
                                    <td>{registro.Numero_factura}</td>
                                    <td>{new Intl.NumberFormat('es-MX').format(registro.Precio_factura)}</td>
                                    <td>{registro.Orden_compra}</td>
                                    <td>{registro.Referencia}</td>
                                </tr>
                            )
                        })
                        :
                        ""
                }
                </tbody>  
            </table>
        );
    }
}

export const VistaPreviaReferencias = ({ 
    agencia, 
    data, 
    isShowedTable, 
    handleStateButtonUpdateLote,
    isActiveButtonActualizar,
    isActiveButtonExcel }) => {
    
    const tableRef = useRef();
    let url = '';
    
    const onUpdateLote = async() => {
        url = ApiUrl + "api/referencia/updatelotecliente"
        const body = { agencia: agencia, lote: data }
        const updateLotePromise = await axiosPatchService( url, body );

        if ( updateLotePromise.isUpdated ) {
            toast('Las Referencias han sido actualizadas con éxito.')
            handleStateButtonUpdateLote();
            // onExportToExcel();
        }
        
    }

    const dataExcelFile = () => {
        const selectFieldsdata = data.map((row) => {
            let newRow = {
                "Cliente": row.Nombre_cliente,
                "Marca": row.Marca,
                "Unidad": row.Unidad,
                "Serie": row.VIN,
                "No. Factura": row.Numero_factura,
                "Precio Factura": row.Precio_factura,
                "Orden Compra": row.Orden_compra,
                "Referencia": row.Referencia
            }
            return newRow;
        })

        return selectFieldsdata;
    }

    const onExportToExcel = () => {
        const dataExcel = dataExcelFile();
        const Header = ["Cliente", "Marca", "Unidad", "Serie", "No. Factura", "Precio Factura", "Orden Compra", "Referencia"]
        const fileName = "Asignación Contrato";
        const fileNameExtension = "Asignacion_Contrato.xlsx";
        let wb = XLSX.utils.book_new()
        let ws = XLSX.utils.json_to_sheet([])
        XLSX.utils.sheet_add_json(ws, dataExcel, {
            header: Header,
            skipHeader: false,
        })

        XLSX.utils.book_append_sheet(wb, ws, fileName);
        XLSX.writeFile(wb, fileNameExtension)
    }

    return (
        isShowedTable && 
        <div>
            <div className='row m-2'>
                <TablaReferencias data={data} ref={tableRef}/>
            </div>
            <button
            type="button"
            className="btn btn-info mt-2 mb-2 ml-2"
            onClick={onUpdateLote}
            disabled={!isActiveButtonActualizar}
            >
                Actualizar
            </button>
            <button
            title="Exportar a Excel"
            type="button"
            className="btn btn-outline-success mt-2 mb-2 ml-2"
            onClick={onExportToExcel}
            disabled={!isActiveButtonExcel}
            >
                <FontAwesomeIcon icon={faFileExcel} />
                <small className="ml-2">Excel</small>
            </button>
        </div>
    )
}