import React, { useRef, Component, useState } from 'react'

import * as XLSX from "xlsx"
import { toast } from 'react-toastify';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileExcel } from '@fortawesome/free-solid-svg-icons';
import { axiosDeleteServide, axiosPostService } from '../../../../services/asignacionLoteService/AsignacionLoteService';
import { ApiUrl } from '../../../../services/ApiRest';

class TablaLotes extends Component {
    render() {
        const data = this.props.data;
        return (
            <table className='table display compact'>
                <thead style={{backgroundColor:'#1565C0', color:'white'}}>
                    <tr className='text-center'>
                        <th>Cliente</th>
                        <th>Marca</th>
                        <th>Unidad</th>
                        <th>Serie</th>
                        <th>No. Factura</th>
                        <th>Precio Factura</th>
                        <th>Orden Compra</th>
                        {/* <th>Monto Total</th> */}
                    </tr>
                </thead>
                <tbody style={{backgroundColor:'#FFFFE0'}}>
                    {
                        data.length > 0 ?
                            data.map((registro) => {
                                return (
                                    <tr className='text-center'>
                                        <td>{registro.cliente}</td>
                                        <td>{registro.Marca}</td>
                                        <td>{registro.Descripcion}</td>
                                        <td>{registro.Vin}</td>
                                        <td>{registro.Factura}</td>
                                        <td>{new Intl.NumberFormat('es-MX').format(registro.Venta)}</td>
                                        <td>{registro.ordenCompra}</td>
                                        {/* <td>{registro.montoTotal}</td> */}
                                    </tr>
                                )
                            })
                            :
                            ""
                    }
                </tbody>
            </table>
        );
    }
}

export const LoteVehiculosTable = ({
    data,
    agencia,
    isShowedTable,
    Folio_lote,
    isEditMode,
    isActiveButtonGuardarLote,
    isActiveButtonActualizarLote,
    handleStateButtonGuardarLote }) => {

    const tableRef = useRef();
   /*  const [isExcelButtonDisabled, setIsExcelButtonDisabled] = useState(true)
    const [isGuardarLoteButtonDisabled, setIsGuardarLoteButtonDisabled] = useState(false) */
    let url = '';

    const dataExcelFile = () => {
        const selectFieldsdata = data.map((row) => {
            let newRow = {
                "Cliente": row.cliente,
                "Marca": row.Marca,
                "Unidad": row.Descripcion,
                "Serie": row.Vin,
                "No. Factura": row.Factura,
                "Precio Factura": (row.Venta),
                "Orden Compra": row.ordenCompra,
                // "Monto Total": row.montoTotal
            }
            return newRow;
        })

        return selectFieldsdata;
    }

    const onGuardarLote = async () => {

        if (isEditMode) {
            url = ApiUrl + "api/deleteLote"
            const deleteLote = { agencia: agencia, Folio_lote: Folio_lote }
            const eliminarLote = await axiosDeleteServide( url, deleteLote );
            
            if ( !eliminarLote.isDeleted ) {
                toast('No fue posible actualizar el lote, favor de volver a intentar.')
                return;
            }

            url = ApiUrl + "api/updateLote"
            const editLote = { agencia: agencia, lote: data, Folio_lote: Folio_lote }
            const result = await axiosPostService(url, editLote);
            if (result.isUpdated) {
                // setIsExcelButtonDisabled(false)
                // setIsGuardarLoteButtonDisabled(true)
                toast('El Lote ha sido actualizado con éxito.')
            }
            if (!result.isUpdated) {
                toast('Error con el servidor, no fue posible actualizar el lote.')
            }
            // onExportToExcel()
            handleStateButtonGuardarLote(isActiveButtonGuardarLote)

        }

        if (!isEditMode) {
            url = ApiUrl + "api/createLote";
            const newLote = { agencia: agencia, lote: data, Folio_lote: Folio_lote }
            const result = await axiosPostService(url, newLote);

            if (result.length === 0) {
                toast('Error al conectar con el servidor.')
                return;
            }

            if (!result.isLoteCreated) {
                toast('Error al crear el lote: Ya existe un nombre de lote idéntico en el sistema.')
                return;
            }

            // setIsExcelButtonDisabled(false)
            /* onExportToExcel() */
            handleStateButtonGuardarLote(isActiveButtonGuardarLote)
            toast('El Lote ha sido creado con éxito')
        }
    }

    const onExportToExcel = () => {
        const dataExcel = dataExcelFile();
        const Header = ["Cliente", "Marca", "Unidad", "Serie", "No. Factura", "Precio Factura", "Orden Compra"] //, "Monto Total"
        const fileName = "Asignación Contrato";
        const fileNameExtension = "Asignacion_Contrato.xlsx";
        let wb = XLSX.utils.book_new()
        let ws = XLSX.utils.json_to_sheet([])

        XLSX.utils.sheet_add_json(ws, dataExcel, {
            header: Header,
            skipHeader: false,
        })

        XLSX.utils.book_append_sheet(wb, ws, fileName);
        XLSX.writeFile(wb, fileNameExtension)

    }

    return (

        isShowedTable &&
        <div>
            <div className='row m-2'>
                <TablaLotes data={data} ref={tableRef} />
            </div>

            <button
                type='button'
                className='btn btn-info  mt-2 mb-2 ml-2'
                onClick={onGuardarLote}
                disabled={data.length === 0 || (!isActiveButtonGuardarLote && !isEditMode) || (!isActiveButtonActualizarLote && isEditMode)}
            >
                {isEditMode ? 'Actualizar Lote' : 'Guardar Lote'}
            </button>

            <button
                onClick={onExportToExcel}
                title='Exportar a excel'
                type='button'
                className="btn btn-outline-success mt-2 mb-2 ml-2"
                disabled={data.length === 0 || (isActiveButtonActualizarLote && isEditMode ) || (isActiveButtonGuardarLote && !isEditMode )}
            >
                <FontAwesomeIcon icon={faFileExcel} />
                <small className='ml-2'>Excel</small>
            </button>
        </div>

    )
}
