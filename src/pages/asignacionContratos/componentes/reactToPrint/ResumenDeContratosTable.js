import React, { useRef, Component, useState } from 'react'

import { useReactToPrint } from 'react-to-print';
import * as XLSX from "xlsx"
import { toast } from "react-toastify"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileExcel, faPrint } from '@fortawesome/free-solid-svg-icons';
import { invertirCadenaFecha } from '../../../../helpers/fecha';
// import '../../../../css/asignacionContratos/asignacionContratos.css'

class SummaryTable extends Component {
    render() {
        const data = this.props.data;
        const color = this.props.colorHeader;
        return (
            <div className='mr-2'>
            <table className='table display compact'>
                <thead style={{backgroundColor:'#1565C0', color:`${color}`}}>
                    <tr className='text-center' style={{ fontSize: "12px" }}>
                        <th>Cliente</th>     
                        <th>Marca</th>     
                        <th>Unidad</th>     
                        <th>VIN</th>     
                        <th>No. Factura</th>     
                        <th>Precio Factura</th>     
                        <th>Orden Compra</th>     
                        <th>Referencia</th>
                        <th>Folio Compra Contrato</th>
                        <th>Fecha Compra Contrato</th>
                        <th>Monto Plan Piso</th>
                        <th>Monto Depósito Cuenta Cheques</th>
                        <th>Tasa</th>
                        <th>Monto Total</th>
                        <th>Inversión Inicial</th>
                    </tr>
                </thead>
                <tbody style={{backgroundColor:'#FFFFE0'}}>
                    {
                        data.length > 0 ?
                        data.map((registro) => {
                            return (
                                <tr className='text-center' style={{ fontSize: "12px" }}>
                                  <td>{registro.Nombre_cliente}</td>
                                  <td>{registro.Marca}</td>
                                  <td>{registro.Unidad}</td>
                                  <td>{registro.VIN}</td>
                                  <td>{registro.Numero_factura}</td>
                                  <td>{new Intl.NumberFormat('es-MX').format(registro.Precio_factura)}</td>
                                  <td>{registro.Orden_compra}</td>
                                  <td>{registro.Referencia}</td>
                                  <td>{registro.Folio_compra_contrato}</td>
                                  <td>{invertirCadenaFecha(registro.Fecha_compra_contrato.substring(0,10))}</td>
                                  <td>{new Intl.NumberFormat('es-MX').format(registro.Monto_plan_piso)}</td>
                                  <td>{new Intl.NumberFormat('es-MX').format(registro.Monto_deposito_cuenta_cheques)}</td>
                                  <td>{registro.Tasa_porcentaje_enganche}</td>
                                  <td>{new Intl.NumberFormat('es-MX').format(registro.Monto_total)}</td>
                                  <td>{new Intl.NumberFormat('es-MX').format(registro.Inversion_inicial)}</td>
                                </tr>
                            )
                        })
                        :
                        ""
                    }
                </tbody>
            </table>
            </div>
        );
    }
}

class SummaryTableToPrint extends Component {
    render() {
        const data = this.props.data;
        const color = this.props.colorHeader;
        return (
            <div className='m-2 p-2'> {/* mr-2 paddings*/}
            <table className='table display compact' style={{width:'100%'}}>
                <thead style={{backgroundColor:'#1565C0', color:`${color}`}}>
                    <tr className='text-center' style={{ fontSize: "10px" }}>
                        <th style={{width:'5%'}}>Cliente</th>     
                        <th style={{width:'5%'}}>Marca</th>     
                        <th style={{width:'5%'}}>Unidad</th>     
                        <th style={{width:'5%'}}>VIN</th>     
                        <th style={{width:'5%'}}>No. Factura</th>     
                        <th style={{width:'5%'}}>Precio Factura</th>     
                        <th style={{width:'5%'}}>Orden Compra</th>     
                        <th style={{width:'5%'}}>Referencia</th>
                        <th style={{width:'5%'}}>Folio Compra</th>
                        <th style={{width:'10%'}}>Fecha Compra</th>
                        <th style={{width:'5%'}}>Plan Piso</th>
                        <th style={{width:'5%'}}>Monto Depósito Cuenta Cheques</th>
                        <th style={{width:'5%'}}>Tasa</th>
                        <th style={{width:'5%'}}>Monto Total</th>
                        <th style={{width:'5%'}}>Inversión Inicial</th>
                    </tr>
                </thead>
                <tbody style={{backgroundColor:'#FFFFE0'}}>
                    {
                        data.length > 0 ?
                        data.map((registro) => {
                            return (
                                <tr className='text-center' style={{ fontSize: "9px" }}>
                                  <td style={{width:'5%'}}>{registro.Nombre_cliente}</td>
                                  <td style={{width:'5%'}}>{registro.Marca}</td>
                                  <td style={{width:'5%'}}>{registro.Unidad}</td>
                                  <td style={{width:'5%'}}>{registro.VIN}</td>
                                  <td style={{width:'5%'}}>{registro.Numero_factura}</td>
                                  <td style={{width:'5%'}}>{registro.Precio_factura}</td>
                                  <td style={{width:'5%'}}>{registro.Orden_compra}</td>
                                  <td style={{width:'5%'}}>{registro.Referencia}</td>
                                  <td style={{width:'5%'}}>{registro.Folio_compra_contrato}</td>
                                  <td style={{width:'10%'}}>{invertirCadenaFecha(registro.Fecha_compra_contrato.substring(0,10))}</td>
                                  <td style={{width:'5%'}}>{registro.Monto_plan_piso}</td>
                                  <td style={{width:'5%'}}>{registro.Monto_deposito_cuenta_cheques}</td>
                                  <td style={{width:'5%'}}>{registro.Tasa_porcentaje_enganche}</td>
                                  <td style={{width:'5%'}}>{registro.Monto_total}</td>
                                  <td style={{width:'5%'}}>{registro.Inversion_inicial}</td>
                                </tr>
                            )
                        })
                        :
                        ""
                    }
                </tbody>
            </table>
            </div>
        );
    }
}

export const ResumenDeContratosTable = ({ data }) => {
    const [colorHeader, setColorHeader] = useState("white")
    const tableRef = useRef();
    let t;

    const dataExcelFile = () => {
        const selectFieldsData = data.map((row) => {
            let newRow = {
                "Cliente": row.Nombre_cliente,
                "Marca": row.Marca,
                "Unidad": row.Unidad,
                "Serie": row.VIN,
                "No. Factura": row.Numero_factura,
                "Precio Factura": row.Precio_factura,
                "Orden Compra": row.Orden_compra,
                "Referencia": row.Referencia,
                "Folio compra contrato": row.Folio_compra_contrato,
                "Fecha compra contrato": invertirCadenaFecha(row.Fecha_compra_contrato),
                "Monto plan piso": row.Monto_plan_piso,
                "Monto deposito cuenta cheques": row.Monto_deposito_cuenta_cheques,
                "Tasa": row.Tasa_porcentaje_enganche,
                "Monto total": row.Monto_total,
                "Inversion inicial": row.Inversion_inicial
            }
            return newRow;
        })
        return selectFieldsData;
    }

    const onExportToExcel = () => {
        const dataExcel = dataExcelFile();
        const Header = ["Cliente", "Marca", "Unidad", "Serie", "No. Factura", "Precio Factura", "Orden Compra", "Referencia", "Folio compra contrato", "Fecha compra contrato", "Monto plan piso", "Monto deposito cuenta cheques", "Tasa", "Monto total", "Inversion inicial"];
        const fileName = "Resumen Contrato";
        const fileNameExtension = "Resumen_Contrato.xlsx";
        let wb = XLSX.utils.book_new()
        let ws = XLSX.utils.json_to_sheet([])
        XLSX.utils.sheet_add_json(ws, dataExcel, {
            header: Header,
            skipHeader: false,
        })

        XLSX.utils.book_append_sheet(wb, ws, fileName);
        XLSX.writeFile(wb, fileNameExtension)
    }

    const handlePrint = useReactToPrint({
        content: () => tableRef.current
    })

    const changeHeaderColor = () => {
        setColorHeader("black");
        clearTimeout(t);
        t = setTimeout(imprimir, 500);
    }

    const imprimir = () => {
        handlePrint();
        setColorHeader("white");
    }

    return (
        <>
          <div className=' table-responsive'> {/* row m-2 */}
        
                {/* <SummaryTable data={data} ref={tableRef} colorHeader={colorHeader}/> */}
                <SummaryTable data={data} colorHeader={colorHeader}/> {/* ref={tableRef} */}
                <style type="text/css" media="print">{"\
                  @page {\ size: landscape; margin: 4 0 14 0 !important;\ }\
                "}<SummaryTableToPrint data={data} ref={tableRef} colorHeader={colorHeader}/></style>
                {/* </SummaryTable> */}

          </div>
          <div className='row m-2'>
            <button
            title='Imprimir'
            type='button'
            className='btn btn-outline-dark mt-2 mb-2 ml-2'
            disabled={data.length === 0}
            onClick={() => {
                changeHeaderColor();
            }}
            >
                <FontAwesomeIcon icon={faPrint}/>
                <small className="ml-2">Imprimir</small>
            </button>
            <button
            title="Exportar a Excel"
            type="button"
            className="btn btn-outline-success mt-2 mb-2 ml-2"
            onClick={onExportToExcel}
            disabled={data.length === 0}
            >
                <FontAwesomeIcon icon={faFileExcel} />
                <small className="ml-2">Excel</small>   
            </button>
            </div>
        </>
    )
}
