import React, { useState, useEffect } from 'react'
import { toast } from 'react-toastify'

import { VistaPreviaReferencias } from './reactToPrint/VistaPreviaReferencias'
import { ApiUrl } from '../../../services/ApiRest'
import { invertirCadenaFecha } from '../../../helpers/fecha'
import '../../../css/asignacionContratos/asignacionReferencia/asignacionReferencia.css'
import { axiosPostService } from '../../../services/asignacionLoteService/AsignacionLoteService'

const AsignacionReferencia = ({ agencia, clientes }) => {
  const [VINClientes, setVINClientes] = useState([])
  const [NombresLoteCliente, setNombresLoteCliente] = useState([])
  const [isShowedTable, setIsShowedTable] = useState(false)
  const [Folio_lote_edit, setFolio_lote_edit] = useState(-1)
  const [isActiveButtonExcel, setisActiveButtonExcel] = useState(true)
  const [isActiveButtonActualizar, setisActiveButtonActualizar] = useState(false)
  const [nombreClienteState, setnombreClienteState] = useState("")
  const [ubicacionState, setUbicacionState] = useState("")
  const [nombreLoteState, setnombreLoteState] = useState("")
  const [numClienteState, setNumClienteState] = useState(-1)
  const [inputsObject, setInputsObject] = useState({
    NombreCliente: "",
    NombreLote: "",
    Ubicacion: "",
    numCliente: 0
  })
  let url = '';

  useEffect(() => {
    getNombresLoteCliente( clientes[0] )
  }, [clientes])
  
  const getNombresLoteCliente = async (cliente) => {
    const { Nombre_corto, Ubicacion, Num_cliente } = cliente;
    setInputsObject({...inputsObject, NombreCliente: Nombre_corto, Ubicacion: Ubicacion, numCliente: Num_cliente})

    url = ApiUrl + "api/referencia/getNombresLotesCliente";
    const body = { agencia: agencia, Nombre_corto: Nombre_corto, Ubicacion: Ubicacion, numCliente: Num_cliente }
    const nombres_lote_cliente = await axiosPostService( url, body );
    if ( nombres_lote_cliente.length === 0 ) {
      resetValuesVINClientes()
      return;
    }

    const { Folio_lote, NumCliente } = nombres_lote_cliente[0];
    setNombresLoteCliente([]);
    setNombresLoteCliente( nombres_lote_cliente );
    getLoteCliente( Folio_lote, Nombre_corto, NumCliente );
  }
  

  const getLoteCliente = async (Folio_lote, Nombre_corto, NumCliente) => {
    setFolio_lote_edit( Folio_lote );
    url = ApiUrl + "api/referencia/getLoteCliente";
    const body_lote = { agencia: agencia, Folio_lote: Folio_lote, NumCliente: NumCliente };
    const vins_lote = await axiosPostService( url, body_lote );
    setVINClientes( vins_lote )
  }

  const resetValuesVINClientes = () => {
    setVINClientes([]);
    setNombresLoteCliente([])
    // toast("No se han encontrado lotes disponibles para asignación de referencias.");
  }

  const changeSelectClientes = (e) => {
    if ( isActiveButtonActualizar ) setisActiveButtonActualizar(false)
    if ( !isActiveButtonExcel )  setisActiveButtonExcel(true)
    setIsShowedTable(false)
    const split = e.target.value.split("|")
    const [ Ubicacion, Nombre_cliente, Num_cliente ] = split;
    setInputsObject({...inputsObject, NombreCliente: Nombre_cliente, Ubicacion: Ubicacion, numCliente: Num_cliente})
    const objetoCliente = { Nombre_corto: Nombre_cliente, Ubicacion: Ubicacion, Num_cliente: Num_cliente }
    getNombresLoteCliente( objetoCliente )
  }

  const changeSelectNombresLoteCliente = (e) => {
    if ( isActiveButtonActualizar ) setisActiveButtonActualizar(false)
    if ( !isActiveButtonExcel )  setisActiveButtonExcel(true)
    setIsShowedTable( false );
    const Folio_lote = e.target.value;
    const Nombre_cliente = inputsObject.NombreCliente;
    const Num_Cliente = inputsObject.numCliente;
    setFolio_lote_edit(Folio_lote)
    getLoteCliente( Folio_lote, Nombre_cliente, Num_Cliente)
  }

  const onChange = (e, vinRegistro) => {
    

    const updateListaClientes = VINClientes.map((row) => {
      if ( row.VIN === vinRegistro ) {
        let updaterow = {
          ...row,
          Referencia: e.target.value
        }
        return updaterow
      }
      return row
    })

    setVINClientes(updateListaClientes)

    if ( !isActiveButtonActualizar ) setisActiveButtonActualizar(true)
    if ( isActiveButtonExcel ) setisActiveButtonExcel(false)
    
  }

  const handleStateButtonUpdateLote = () => {
    if ( isActiveButtonActualizar ) setisActiveButtonActualizar(false)
    if ( !isActiveButtonExcel )  setisActiveButtonExcel(true)
    
    /* const cliente = getObjectClienteSelected();
    getNombresLoteCliente(cliente) */
  }

  const getObjectClienteSelected = () => {
    // setInputsObject({...inputsObject, NombreCliente: Nombre_cliente, Ubicacion: Ubicacion, numCliente: Num_cliente})
    // let nombre_cliente = nombreCliente;
    // let cliente = clientes.find((obj) => obj.Nombre_corto === nombre_cliente)
    let numero_cliente = inputsObject.numCliente;
    let cliente = clientes.find((obj) => obj.Num_cliente == numero_cliente)
    return cliente;
  }

  const onGenerateTable = () => {
    setIsShowedTable(!isShowedTable)
  }

  return (
    <>
      <div className="row m-2">
        <div className="col-6">
          <div className="row d-flex justify-content-between pl-2 pr-4">
              <h6 className='mr-4'>1.- Seleccionar Cliente: </h6>
              <select className='form-select select-class-1' onChange={(e) => changeSelectClientes(e)}>
                {clientes.map(cliente => {
                  return (
                    <option value={`${cliente.Ubicacion}|${cliente.Nombre_corto}|${cliente.Num_cliente}`} selected={cliente.Id === 1}>{`${cliente.Nombre_corto} ${cliente.Ubicacion}`}</option>
                  )
                })}
              </select>
          </div>
        </div>
      </div>
      <div className="row m-2 bottom-space">
        <div className="col-6" >
          <div className="row d-flex justify-content-between pl-2 pr-4">
              <h6 className='mr-4'>2.- Nombre de Lote: </h6>
                <select className='form-select select-class-1' onChange={(e) => changeSelectNombresLoteCliente(e)} disabled={ NombresLoteCliente.length === 0 }>
                 { 
                    NombresLoteCliente.map(objeto => {
                      return (
                        <option value={objeto.Folio_lote} selected={NombresLoteCliente[0].Nombre_lote == objeto.Nombre_lote}>{ `${objeto.Nombre_lote} ${invertirCadenaFecha(objeto.Fecha_elaboracion.substring(0, 10))}` }</option>
                      )
                    })
                 }
                </select>
          </div>
        </div>
      </div>
      <div className="row m-2">
        <h6>3.- Asignar Referencias</h6>
      </div>
      {
        !isShowedTable &&
        <div className="row m-2">
        <table className='table display compact'>
          <thead style={{backgroundColor:'#1565C0', color:'white'}}>
            <tr className='text-center'>
              <th>Cliente</th>     
              <th>Marca</th>     
              <th>Unidad</th>     
              <th>VIN</th>     
              <th>No. Factura</th>     
              <th>Precio Factura</th>     
              <th>Orden Compra</th>     
              <th>Referencia</th>     
            </tr>
          </thead>
          <tbody style={{backgroundColor:'#FFFFE0'}}>
            {
              VINClientes.length > 0 ?
              VINClientes.map((registro) => {
                return (
                  <tr className='text-center'>
                    <td>{registro.Nombre_cliente}</td>
                    <td>{registro.Marca}</td>
                    <td>{registro.Unidad}</td>
                    <td>{registro.VIN}</td>
                    <td>{registro.Numero_factura}</td>
                    <td>{new Intl.NumberFormat('es-MX').format(registro.Precio_factura)}</td>
                    <td>{registro.Orden_compra}</td>
                    <td>
                      <input type="text" className="form-control" onChange={(e) => onChange(e, registro.VIN)} value={registro.Referencia} name="referencia" autoComplete='off' />
                    </td>
                  </tr>
                )
              })
              :
              <tr className='p-2'>
                <strong>No existen lotes creados del cliente seleccionado</strong>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>

            }
          </tbody>
        </table>
      </div>
      }
      <VistaPreviaReferencias
      agencia={ agencia }
      data={ VINClientes }
      isShowedTable={isShowedTable}
      // Folio_lote={Folio_lote_edit}
      handleStateButtonUpdateLote={handleStateButtonUpdateLote}
      isActiveButtonExcel={isActiveButtonExcel}
      isActiveButtonActualizar={isActiveButtonActualizar}
      />
      <div className="row m-2">
      <button
        type='button'
        className='btn btn-info mt-2 mb-2'
        onClick={onGenerateTable}
        disabled={VINClientes.length === 0}
      >
        { isShowedTable ? "Editar" : "Vista Previa"}
      </button>
      </div>
    </>
  )
}

export default AsignacionReferencia