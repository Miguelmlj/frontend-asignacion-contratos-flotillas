import React,{ useState, useEffect } from 'react'

import { ApiUrl } from '../../../services/ApiRest';
import { getAgencia } from '../../../helpers/getAgencia';
import { axiosPostService } from '../../../services/asignacionLoteService/AsignacionLoteService';
import { LoteVehiculosTable } from './reactToPrint/LoteVehiculosTable';
import { invertirCadenaFecha } from '../../../helpers/fecha';
import 'react-toastify/dist/ReactToastify.css';
import '../../../css/asignacionContratos/asignacionLote/asignacionLote.css'
import { toast } from 'react-toastify';

const AsignacionLote = ({ agencia, Folio_lote, clientes, incrementarFolioLote }) => {
  const [VINClientes, setVINClientes] = useState([])
  const [updateVentasFlotillasDMSTable, setUpdateVentasFlotillasDMSTable] = useState(false)
  const [VINClientesGenerados, setVINClientesGenerados] = useState([])
  const [NombresLoteCliente, setNombresLoteCliente] = useState([])
  const [VINSLote, setVINSLote] = useState([]) //considerar si se necesitará este estado.
  const [isShowedTable, setIsShowedTable] = useState(false)
  const [isEditMode, setisEditMode] = useState(true)
  const [isActiveButtonGuardarLote, setIsActiveButtonGuardarLote] = useState(true)
  const [isActiveButtonActualizarLote, setisActiveButtonActualizarLote] = useState(false)
  const [Folio_lote_edit, setFolio_lote_edit] = useState(-1)
  const [nombreCliente, setNombreCliente] = useState("")
  const [ubicacionState, setUbicacionState] = useState("")
  const [nombreLoteState, setNombreLoteState] = useState("")
  const [ordenCompraState, setOrdenCompraState] = useState("")
  const [NumCliente, setNumCliente] = useState(0)
  const [inputsObject, setInputsObject] = useState({
    NombreCliente: "", 
    NombreLote: "",
    OrdenCompra: "",
    Ubicacion: "",
    numCliente: 0
  })
  let url = '';
  const vin_selected = 'vin_selected'
  const orden_compra_selected = 'orden_compra_selected'
  
  useEffect( () => {
      /* TODO: validar cuando no haya clientes en la lista. (Otras Empresas.) */
      updateTablaVentasFlotillasDMS()
    //  getNombresLoteCliente( clientes[0] )
    console.log(clientes);
  }, [clientes])
  
  const updateTablaVentasFlotillasDMS = async () => {

      if ( clientes.length > 0 ) {
      url = ApiUrl + "api/updateregistrosoracle"
      const body =  { agencia: agencia }
      const response = await axiosPostService( url, body )
      if ( response.isUpdated ) {
          setUpdateVentasFlotillasDMSTable( true )
          getNombresLoteCliente( clientes[0] )
        
      }
    }
  }

  const getLoteCliente = async (Folio_lote, Nombre_corto, NumCliente) => {
      setFolio_lote_edit( Folio_lote )
      url = ApiUrl + "api/getLoteCliente"
      const body_lote = { agencia: agencia, Folio_lote: Folio_lote, NumCliente: NumCliente };
      const vins_lote = await axiosPostService( url, body_lote );

      url = ApiUrl + "api/getvinscliente";
      const body_cliente = { Agencia: agencia, Nombre_cliente: Nombre_corto, Folio_lote: Folio_lote, NumCliente: NumCliente };
      const total_vines_cliente = await axiosPostService( url, body_cliente )

      if ( total_vines_cliente.length === 0 ) {
        toast('No existen registros en la Base de Datos para el cliente seleccionado.')
        return;
      }

      //TODO: !IMPORTANT : AGREGAR VARIABLES NO EXISTENTES : MARCA, [ORDENCOMPRA, NOMBRELOTE]=>SE agregan al seleccionar.
      const add_properties_total_vines_cliente = addBooleansVariables( total_vines_cliente, Nombre_corto );
      mergeVinsLoteVinsCliente(vins_lote, add_properties_total_vines_cliente, Nombre_corto)

  }

  const addBooleansVariables = ( total_vines_cliente, Nombre_corto ) => {
    let add_properties_total_vines_cliente = total_vines_cliente.map((objcliente) => {
      let newObj = {
        ...objcliente,
        Marca: 'CHEVROLET', 
        ordenCompraSelected: false, 
        vinSelected: false, 
        vinBlocked: false,
        cliente: Nombre_corto
      }
      return newObj
    })
    return add_properties_total_vines_cliente;
  }

  const mergeVinsLoteVinsCliente = ( vins_lote, total_vines_cliente, Nombre_corto ) => {
    let VinsEnLote = []
    const merged_vins = total_vines_cliente.map((objcliente) => {
      const findVinInLote = vins_lote.find(objlote => objlote.VIN === objcliente.Vin)
      const newObjCliente = isVinInLote(objcliente, findVinInLote, Nombre_corto);
      if ( findVinInLote !== undefined ) VinsEnLote.push(newObjCliente);
      return newObjCliente;

    })


    //TODO:  ¿Cuando será asignado el nombre del lote? al ESTADO INPUTSOBJECT??
    let valor_nombre_lote = vins_lote.length > 0 ? vins_lote[0].Nombre_lote : "";
    setVINClientes(merged_vins)
    setVINClientesGenerados(VinsEnLote)
    setNombreLoteState(valor_nombre_lote)
    setInputsObject({
      ...inputsObject, 
      NombreLote: valor_nombre_lote, 
      OrdenCompra: ""})
  }

  const isVinInLote = ( objcliente, findVinInLote, Nombre_corto ) => {
    //TODO: EL cliente no es el mismo en los registros totales.
    let objeto = {}
    if ( findVinInLote !== undefined ) {
      objeto = {
        // ...objcliente,
        //add
        Descripcion:objcliente.Descripcion,
        Factura: objcliente.Factura,
        NumCliente: objcliente.NumCliente,
        Venta: findVinInLote.Precio_factura,
        Vin: objcliente.Vin,
        //add
        Ubicacion: findVinInLote.Ubicacion_cliente,
        cliente: findVinInLote.Nombre_cliente,
        // montoTotal: findVinInLote.Monto_total,
        nombreLote: findVinInLote.Nombre_lote,
        // numeroFactura: findVinInLote.Numero_factura,
        ordenCompra: findVinInLote.Orden_compra,
        Marca: 'CHEVROLET',
        ordenCompraSelected: findVinInLote.Orden_compra !== "" ? true : false,
        // precioFactura: findVinInLote.Precio_factura,
        vinSelected: true,
        vinBlocked: true
      }
    }
    if ( findVinInLote === undefined ) {
      objeto = {
        // ...objcliente, 
        cliente:Nombre_corto,
        Descripcion: objcliente.Descripcion,
        Factura: objcliente.Factura,
        NumCliente: objcliente.NumCliente,
        Venta: (objcliente.Venta * 1.16), //incrementr el iva
        Vin: objcliente.Vin,
        Marca: 'CHEVROLET',
        ordenCompraSelected: objcliente.ordenCompraSelected, 
        vinSelected: objcliente.vinSelected,
        vinBlocked: objcliente.vinBlocked

      }
    }
    return objeto;
  }

  const handleStateButtonGuardarLote = (value) => {

    setInputsObject({...inputsObject, OrdenCompra: "" })
    if ( isEditMode ) {
      // setIsActiveButtonGuardarLote(!value)
      setisActiveButtonActualizarLote(false)
      const Folio_lote = Folio_lote_edit;
      const Nombre_cli = nombreCliente;
      const Num_Cliente = NumCliente; 
      getLoteCliente( Folio_lote, Nombre_cli, Num_Cliente )
      // setIsShowedTable( false )
    }

    if ( !isEditMode ) {
      // setIsShowedTable( false )
      setisActiveButtonActualizarLote(false)
      setIsActiveButtonGuardarLote(false)
      incrementarFolioLote();
      setisEditMode(!isEditMode)
      const cliente = getObjectClienteSelected()
      getNombresLoteCliente(cliente);
    }
  }

  const getNombresLoteCliente = async (cliente) => {
    //TODO: VALIDAR si no hay registros de clintes..
    //toast..
      const { Nombre_corto, Ubicacion, Num_cliente } = cliente;
      setInputsObject({...inputsObject, NombreCliente: Nombre_corto, Ubicacion: Ubicacion, numCliente: Num_cliente})
      setNombreCliente(Nombre_corto)
      setNumCliente(Num_cliente);
      setUbicacionState(Ubicacion)

      url = ApiUrl + "api/getNombresLotesCliente";
      const body =  { agencia: agencia, Nombre_corto: Nombre_corto, Ubicacion: Ubicacion, numCliente: Num_cliente }
      const nombres_lote_cliente = await axiosPostService( url, body )

      if ( nombres_lote_cliente.length === 0 ){
         resetValuesVINClientes()
         return;
      }

      const { Folio_lote, NumCliente } = nombres_lote_cliente[0];
      setNombresLoteCliente( nombres_lote_cliente )
      getLoteCliente( Folio_lote, Nombre_corto, NumCliente )
    
  }

  const resetValuesVINClientes = () => {
    setVINClientesGenerados([])
    setVINClientes( [] )
    //TODO: agregue esta linea: reseteamos nombres de lote array
    setNombresLoteCliente([])
    // toast("No se han encontrado lotes creados del cliente seleccionado.");
  
  }

  const changeSelectClientes = async(e) => {
    const split = e.target.value.split("|")
    const [ Ubicacion, Nombre_cliente, Num_cliente ] = split;
    
    if ( !isEditMode ) {
      // const vins_disponibles_cliente = await cargarVinesCliente(Nombre_cliente)
      setInputsObject({...inputsObject, NombreCliente: Nombre_cliente, Ubicacion: Ubicacion, numCliente: Num_cliente})
      setNombreCliente( Nombre_cliente );
      setNumCliente( Num_cliente );
      setUbicacionState( Ubicacion );
      await cargarVinesCliente(Nombre_cliente, Num_cliente);
      
    }

    if ( isEditMode ) {
      const objetoCliente = { Nombre_corto: Nombre_cliente, Ubicacion: Ubicacion, Num_cliente: Num_cliente}
      getNombresLoteCliente(objetoCliente);
      setIsActiveButtonGuardarLote( true );
      setisActiveButtonActualizarLote(false)
      setIsShowedTable( false );
    }
    /* 
     */
  
  }

  const cargarVinesCliente = async (Nombre_cliente, Num_cliente) => {
    url = ApiUrl + "api/getvinsdisponiblescliente"
    const body = { Agencia: agencia, Nombre_cliente: Nombre_cliente, NumCliente: Num_cliente}
    const vins_disponibles_cliente = await axiosPostService( url, body );
    // return vins_disponibles_cliente;
    if ( vins_disponibles_cliente.length === 0 ) {
      setVINClientes([])
      setVINClientesGenerados([])
      setInputsObject({ ...inputsObject,NombreLote: "", OrdenCompra: "", })
      toast("No existen vines disponibles en Base de Datos para el cliente seleccionado")
      setIsShowedTable(false)
    }
    if ( vins_disponibles_cliente.length > 0 ) {
      const add_properties_total_vines_cliente = addBooleansVariables( vins_disponibles_cliente, Nombre_cliente );
      setVINClientes(add_properties_total_vines_cliente)
      setVINClientesGenerados([])
      
      setInputsObject({
        ...inputsObject,
        NombreLote: "",
        OrdenCompra: "",
      })
      setIsShowedTable(false)
    }
  }

  const handleCheckSelected = (e, registro) => {
    if ( isEditMode && !isActiveButtonActualizarLote) setisActiveButtonActualizarLote(true) //setisActiveButtonActualizarLote(false)
    if ( isEditMode && isActiveButtonGuardarLote) setIsActiveButtonGuardarLote(false)
    if ( e.target.value === 'vin_selected' ) {
      updateValuesVIN(registro, e.target.value, e.target.checked)
      return
    }

    if ( e.target.value === 'orden_compra_selected' ) {
      updateValuesVIN(registro, e.target.value, e.target.checked)
    }

  }

  const updateValuesVIN = (registro, value, checked) => {
    const updateVIN = VINClientes.map((row) => {
      if ( registro.Vin === row.Vin ) {
        let updateRow = {}
        if ( value === vin_selected){
          updateRow = { 
            ...row, 
            vinSelected: checked,
            Ubicacion: !checked ? "" : ubicacionState, //inputsObject.Ubicacion 
            nombreLote: !checked ? "" : inputsObject.NombreLote, 
            ordenCompra: !checked ? "" : row.ordenCompra === undefined ? "" : row.ordenCompra, 
            ordenCompraSelected: !checked && checked
          }
          insertNewVIN(updateRow, value, checked) 
          return updateRow
        } 

        if ( value === orden_compra_selected) {
          updateRow = { 
            ...row, 
            ordenCompra: !checked ? "" : inputsObject.OrdenCompra, 
            ordenCompraSelected: checked 
          }
          insertNewVIN(updateRow, value, checked)  
          return updateRow
        }

      }

      return row
    })

    setVINClientes(updateVIN)
  }

  const insertNewVIN = (object, value, checked) => {
    if ( value === vin_selected){
      if ( !checked ) {
        const updateList = VINClientesGenerados.filter((row) => {
            return row.Vin !== object.Vin
        })

        setVINClientesGenerados(updateList)
        return;
      }
      setVINClientesGenerados([
        ...VINClientesGenerados,
        object
      ])
    }

    if ( value === orden_compra_selected) {
        const updateList = VINClientesGenerados.map((row) => {
          if ( object.Vin === row.Vin ) {
            let updateRow = {
              ...row,
              ordenCompra: !checked ? "" : object.ordenCompra, 
              ordenCompraSelected: !checked ? false : true
            }
            return updateRow
          }
          return row
        })
        setVINClientesGenerados(updateList)
    }

  }

  const handleInputsObject = (e) => {
    setInputsObject({
      ...inputsObject,
      [e.target.name]: e.target.value 
    })
  }

  const onGenerateTable = () => {
    setIsShowedTable(!isShowedTable)
  }

  const changeSelectNombresLoteCliente = (e) => {
    if ( isEditMode && isActiveButtonActualizarLote ) setisActiveButtonActualizarLote(false)
    setIsActiveButtonGuardarLote( true )
    setIsShowedTable( false )
    const Folio_lote = e.target.value;
    const Nombre_cliente = nombreCliente; 
    const Num_Cliente = NumCliente;
    setFolio_lote_edit(Folio_lote)
    getLoteCliente( Folio_lote, Nombre_cliente, Num_Cliente );
  }

  const onCreateOrModify = async () => {
    setIsShowedTable(false)
    setIsActiveButtonGuardarLote(true)
    
     if ( !isEditMode ) { //cambiar a modo edicion
      setisActiveButtonActualizarLote(false) //=================================
      const cliente = getObjectClienteSelected()
      getNombresLoteCliente(cliente);
     }

    if ( isEditMode ) { //crear nuevo lote
      
      url = ApiUrl + "api/getvinsdisponiblescliente"
      const body = { Agencia: agencia, Nombre_cliente: nombreCliente, NumCliente: NumCliente}
      const vins_disponibles_cliente = await axiosPostService( url, body );

      if ( vins_disponibles_cliente.length === 0 ) {
        setVINClientes([])
        setVINClientesGenerados([])
        setInputsObject({ ...inputsObject,NombreLote: "", OrdenCompra: "", })
        setisEditMode(!isEditMode)
        toast("No existen vines disponibles en Base de Datos para el cliente seleccionado.")
        return;
      }

      const add_properties_total_vines_cliente = addBooleansVariables( vins_disponibles_cliente, nombreCliente );
      setVINClientes(add_properties_total_vines_cliente)
      setVINClientesGenerados([])
      setisActiveButtonActualizarLote(false) //=================================

      setInputsObject({
        ...inputsObject,
        NombreLote: "",
        OrdenCompra: "",
      })
    }

    setisEditMode(!isEditMode)
  }

  const getObjectClienteSelected = () => {
    // let nombre_cliente = nombreCliente;
    // let cliente = clientes.find((obj) => obj.Nombre_corto === nombre_cliente)
    let numero_cliente = NumCliente;
    let cliente = clientes.find((obj) => obj.Num_cliente == numero_cliente)
    return cliente;
  }

  const disabledVINSelected = (vinBlocked) => {

    if ( isEditMode ) {
    if ( vinBlocked ) return true;
    if ( NombresLoteCliente.length === 0) return true;
    // if ( isEditMode ) {  return false;}
    // if ( inputsObject.NombreLote === "" ){  return false;}
    return false;
    }

    if ( !isEditMode ) {
      if ( inputsObject.NombreLote === "" ) return true;
      return false;
    }

    // return true;

  }
  
  const disabledOrdenCompraSelected = ( vinSelected, vinBlocked, OrdenCompra, OrdenCompraSelected ) => {

    if ( isEditMode  && vinBlocked && OrdenCompra === "" && !OrdenCompraSelected) {
      return false;
    }
    if ( isEditMode && vinBlocked && OrdenCompraSelected) {
      return true;
    }
    if ( isEditMode && !vinSelected) {
      return true;
    }
    if ( isEditMode && vinSelected) {
      return false;
    }
    if ( isEditMode && NombresLoteCliente.length === 0) {
      return true;
    }
    if ( isEditMode ) {
      return false;
    }
    if ( inputsObject.NombreLote !== "" && vinSelected ) {
      return false;
    }
    return true;

  }

  return (
    <>
      <div className="row m-2">
        <div className="col-6">
          <div className="row d-flex justify-content-between pl-2 pr-4">
            {/* <div className="col"> */}
              <h6 className='mr-4'>1.- Seleccionar Cliente: </h6>
            {/* </div> */}
            {/* <div className="col"> */}
              <select className='form-select select-class-1' onChange={(e) => changeSelectClientes(e)} disabled={!updateVentasFlotillasDMSTable} >{/* disabled={ !isEditMode } */}
                {clientes.map(cliente => {
                  return (
                    <option value={`${cliente.Ubicacion}|${cliente.Nombre_corto}|${cliente.Num_cliente}`} selected={cliente.Id === 1}>{`${cliente.Nombre_corto} ${cliente.Ubicacion}`}</option>
                  )
                })}
              </select>
            {/* </div> */}
          </div>
        </div>
      </div>
      <div className="row m-2">
        <div className="col-6" >
          <div className="row d-flex justify-content-between pl-2 pr-4">
            {/* <div className="col"> */}
              <h6 className='mr-4'>2.- Nombre de Lote: </h6>
            {/* </div> */}
            {/* <div className="col"> */}
              {
                !isEditMode ? 
                <input 
                className='input-class' 
                type="text" 
                name="NombreLote" 
                value={inputsObject.NombreLote} 
                onChange={handleInputsObject}
                disabled={!updateVentasFlotillasDMSTable}
                />
                :
                <select className='form-select select-class-1' onChange={(e) => changeSelectNombresLoteCliente(e)} disabled={!updateVentasFlotillasDMSTable || NombresLoteCliente.length === 0}>
                 { 
                    NombresLoteCliente.map(objeto => {
                      return (
                        <option value={objeto.Folio_lote}>{ `${objeto.Nombre_lote} ${invertirCadenaFecha(objeto.Fecha_elaboracion.substring(0, 10))}` }</option>
                      )
                    })
                 }
                </select>

              }
            {/* </div> */}
          </div>
        </div>
      </div>
      <div className="row m-2 bottom-space">
        <div className="col-6" >
          <div className="row d-flex justify-content-between pl-2 pr-4">
            {/* <div className="col"> */}
              <h6 className='mr-4'>3.- Orden de Compra: </h6>
            {/* </div> */}
            {/* <div className="col"> */}
              <input 
              className='input-class' 
              type="text" 
              name="OrdenCompra" 
              value={inputsObject.OrdenCompra} 
              onChange={handleInputsObject}
              disabled={!updateVentasFlotillasDMSTable}
              />
            {/* </div> */}
          </div>
        </div>
      </div>
      <div className="row m-2 d-flex justify-content-between">
        <h6>4.- Seleccionar VIN's que integran el lote</h6>
        <button 
        type='button' 
        className='btn btn-info'
        onClick={onCreateOrModify}
        disabled={!updateVentasFlotillasDMSTable}
        >
          { isEditMode ? "Nuevo Lote" : "Cancelar"}
        </button>
      </div>
      {
        !isShowedTable && 
        <div className="row m-2">
          <table className='table display compact'>
            <thead style={{backgroundColor:'#1565C0', color:'white'}}>
              <tr className='text-center'>
                <th>Unidad</th>
                <th>Marca</th>
                <th>VIN</th>
                <th>Seleccionar VIN</th>
                <th>Orden de Compra</th>
                <th></th>
              </tr>
            </thead>
            <tbody style={{backgroundColor:'#FFFFE0'}}>{/* #FFFACD */}
            {
              VINClientes.length > 0 ?
              VINClientes.map((registro) => {
                return (
                  <tr className='text-center'>
                    <td>{registro.Descripcion}</td>
                    <td>{registro.Marca}</td>
                    <td>{registro.Vin}</td>
                    <td>
                      <input 
                      checked={registro.vinSelected} 
                      className="form-check-input" 
                      name="vin_selected" 
                      onChange={( e ) => handleCheckSelected( e, registro )}
                      type="checkbox" 
                      value="vin_selected"
                      disabled={  disabledVINSelected(registro.vinBlocked) } /* inputsObject.NombreLote === "" */ 
                      />
                    </td>
                    <td className='text-center'> {/* d-flex justify-content-center */}
                      <input 
                      checked={registro.ordenCompraSelected} 
                      className="form-check-input" 
                      name="orden_compra_selected" 
                      onChange={( e ) => handleCheckSelected( e, registro )}
                      type="checkbox" 
                      value="orden_compra_selected" 
                      disabled={ disabledOrdenCompraSelected( registro.vinSelected, registro.vinBlocked, registro.ordenCompra, registro.ordenCompraSelected ) } /* !registro.vinSelected || inputsObject.NombreLote === "" */
                      />
                      
                    </td>
                    <td className='text-left'>
                      <small className='margin-left-10'>{registro.ordenCompra}</small>
                    </td>
                  </tr>
                )
              })
              :
              <tr className='p-2'>
                  {
                    !updateVentasFlotillasDMSTable ?
                    <>
                    <div className="d-flex align-items-start p-2">
                    <strong>Cargando...</strong>
                    <div className='spinner-border ml-2' role="status" aria-hidden="true"></div>
                    </div>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </>
                    :
                    <>
                    <strong>No existen lotes creados del cliente seleccionado</strong>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </>
                  }
              </tr>
            }
            </tbody>
          </table>
      </div>}

      <LoteVehiculosTable 
      agencia={agencia}
      data={VINClientesGenerados} 
      Folio_lote={ isEditMode ? Folio_lote_edit : Folio_lote}
      handleStateButtonGuardarLote={handleStateButtonGuardarLote}
      isActiveButtonGuardarLote={isActiveButtonGuardarLote}
      isActiveButtonActualizarLote={isActiveButtonActualizarLote}
      isEditMode={isEditMode}
      isShowedTable={isShowedTable} 
      />
      
      <div className="row m-2">
        <button 
        type='button' 
        className='btn btn-info mt-2 mb-2' 
        onClick={onGenerateTable}
        disabled={ VINClientesGenerados.length === 0 }
        >
          { isShowedTable ? 'Editar' : 'Vista Previa'}
        </button>
      </div>
      
    </>
  )
}

export default AsignacionLote