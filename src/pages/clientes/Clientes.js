import React, { useState, useEffect } from 'react'
import { ToastContainer, toast } from 'react-toastify';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import axios from 'axios'
import $ from 'jquery'

import { useModal } from '../../modales/shared/useModal';
import Modal from '../../modales/shared/Modal';
import { ApiUrl } from '../../services/ApiRest';
import { getAgencia } from '../../helpers/getAgencia';
import { clientesDataTable } from '../../components/datatable/conf';
import { ModalClientes } from '../../modales/clientes/ModalClientes';
import { ErrorConexion, RegistroExitoso } from '../../constantes/constantesAxios/mensajesAxios';
import '../../css/clientes/clientes.css'

const Clientes = () => {
  const [ isOpenModal, openModal, closeModal ] = useModal(false)
  const [isEditMode, setisEditMode] = useState(false)
  const [data, setData] = useState([])
  const [cliente, setCliente] = useState({
    RFC:"",
    Razon_social:"",
    Nombre_corto:"",
    Num_cliente:"",
    Ubicacion:""
  })
  const agencia = getAgencia();
  let url = "";
  // const ErrorConexion = "Error conexión servidor"
  // const RegistroExitoso = "Registro completado exitosamente"

  useEffect(() => {
    getClientes()
  }, [])
  
  const getClientes = async () => {
    url = ApiUrl + "api/clientes"
    await axios.post(url, agencia)
      .then( response => {
        dataTableDestroy()
        setData(response['data'])
        dataTable()
      })
      .catch( err => {
        toast.error(ErrorConexion)
      })
  }

  const createCliente = () => {
    setisEditMode(false)
    setCliente({...cliente})

    openModal()
  }

  const setValuesWrittenForm = (clienteObject) => {
      isEditMode
      ?
      setCliente({
        RFC           :cliente.RFC,
        Razon_social  :cliente.Razon_social,
        Nombre_corto  :cliente.Nombre_corto,
        Num_cliente   :cliente.Num_cliente,
        Ubicacion     :cliente.Ubicacion,
        Id            :cliente.Id
      })
      :
      setCliente({
        RFC           :clienteObject.RFC,
        Razon_social  :clienteObject.Razon_social,
        Nombre_corto  :clienteObject.Nombre_corto,
        Num_cliente   :clienteObject.Num_cliente,
        Ubicacion     :clienteObject.Ubicacion
      })
  }

  const nuevoRegistroCliente = (clienteObject) => {
    let body = {}
    if ( clienteObject.Num_cliente === "" ) {
      toast.info('El campo número cliente se encuentra vacío')
      setValuesWrittenForm(clienteObject)
      return;
    }
    /* if ( clienteObject.RFC === "" ) {
      toast.info('El campo RFC se encuentra vacío')
      setValuesWrittenForm(clienteObject)
      return;
    } */
    if ( clienteObject.Razon_social === "" ) {
      toast.info('El campo razón social se encuentra vacío')
      setValuesWrittenForm(clienteObject)
      return;
    }
    if ( clienteObject.Nombre_corto === "" ) {
      toast.info('El campo nombre corto se encuentra vacío')
      setValuesWrittenForm(clienteObject)
      return;
    }

    body = {cliente: clienteObject, agencia}

    if (isEditMode) {
      updateClientSelected(body)
      return;
    }

    createClientWritten(body)

  }

  const updateClientSelected = async(body) => {
    url = ApiUrl + "api/clientes/update"
      await axios.patch( url, body )
        .then( response => {
          if ( response['data'].isUpdated ) {
            getClientes();
            setisEditMode(false);
            toast.success(RegistroExitoso);
            closeModal();
          }
          
        })
        .catch( err => {
          toast.error(ErrorConexion)
        })
    
  }

  const editClient = (clientEdit) => {
    setisEditMode(true);
    setCliente({
      RFC:clientEdit.RFC,
      Razon_social:clientEdit.Razon_social,
      Nombre_corto:clientEdit.Nombre_corto,
      Num_cliente:clientEdit.Num_cliente,
      Ubicacion:clientEdit.Ubicacion,
      Id:clientEdit.Id
    });

    openModal();

  }

  const createClientWritten = async ( body ) => {
    url = ApiUrl + "api/clientes/create"
      await axios.post( url, body )
        .then( response => {
          if ( response['data'].isCreated ) {
              getClientes();
              toast.success(RegistroExitoso);
              closeModal();
          }
        })
        .catch( err => {
          toast.error(ErrorConexion)
        })
  }


  const dataTable = () => {
    $('#clientes').DataTable(clientesDataTable);
  }
  
  const dataTableDestroy = () => {
    $('#clientes').DataTable().destroy();
    
  }

  return (
    <div className='content-wrapper'>
      <div className='content-header'>
        <div className='container-fluid'>
          <div className='row mb-4'>
            <div className='col-sm-12'>
              <div className='card card-outline card-primary'>
                <div className='card-header'>
                  <h5 className='m-0 text-dark'>CLIENTES</h5>
                </div>
              </div>    
            </div>  
          </div>
          <div className='margin-left-button'>
            <button
              type='button'
              onClick={createCliente}
              className='btn btn-info mt-4 mb-2'
            >
              Registrar Cliente  
            </button>
          </div>    
        </div>  
      </div>
      <div className='container-fluid'>
        <div className='row ml-2 mr-2'>
          <Modal isOpen={isOpenModal} closeModal={closeModal}>
            <ModalClientes
            data={cliente}
            onSubmit={nuevoRegistroCliente}
            editMode={isEditMode}
            />
          </Modal>

          <div className='table-responsive mb-4'>
            <table id='clientes' className='table table-bordered table-striped compact'>{/* table-striped table-bordered*/}
              <thead>
                <tr>
                  <th className='text-center'># Cliente</th>
                  <th className='text-center'>RFC</th>
                  <th className='text-center'>Razón Social</th>
                  <th className='text-center'>Nombre Corto</th>
                  <th className='text-center'>Ubicación</th>
                  <th className='text-center'>Editar</th>
                </tr>
              </thead>
              <tbody>
                {
                  data.length > 0 ?
                  data.map((cliente) => {
                    return (
                      <tr className='text-center'>
                        <td>{cliente.Num_cliente}</td>
                        <td>{cliente.RFC}</td>
                        <td>{cliente.Razon_social}</td>
                        <td>{cliente.Nombre_corto}</td>
                        <td>{cliente.Ubicacion}</td>
                        <td>
                          <button
                          type='button'
                          className='btn btn-outline-dark'
                          title='Editar'
                          onClick={() => editClient(cliente)}
                          >
                            <FontAwesomeIcon icon={faEdit} />  
                          </button>  
                        </td>
                      </tr>
                    )
                  }) : ""
                }  
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <ToastContainer />
    </div>
  )
}

export default Clientes