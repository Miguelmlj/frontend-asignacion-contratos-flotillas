import React,{ useState, useEffect } from 'react'

import axios from 'axios';
import { toast } from 'react-toastify';
import { axiosPostService } from '../../../services/asignacionLoteService/AsignacionLoteService';
import { useModal } from '../../../modales/shared/useModal';
import Modal from '../../../modales/shared/Modal';
import { ModalOrdenDeCompra } from '../../../modales/ordendecompra/ModalOrdenDeCompra';
import { ordenDeCompraDataTable } from '../../../components/datatable/conf';
import $ from 'jquery';
import { ApiUrl } from '../../../services/ApiRest';
import { ErrorConexion, RegistroExitoso } from '../../../constantes/constantesAxios/mensajesAxios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFilePdf, faEdit } from '@fortawesome/free-solid-svg-icons';
import { saveAs } from 'file-saver';

const OrdenDeCompra1 = ({ agencia }) => {
    let url = '';
    const [ isOpenModal, openModal, closeModal ] = useModal(false);
    const [isEditMode, setisEditMode] = useState(false);
    const [tipoVehiculos, setTipoVehiculos] = useState([])
    const [nombreDeClientes, setNombreDeClientes] = useState([])
    const [data, setData] = useState([])

    const [ordenDeCompra, setOrdenDeCompra] = useState({
        Cliente:"",
        Ubicacion:"",
        OrdenCompra:"",
        Cantidad:"",
        TipoVehiculo:"",
        Existencia:"",
        DocumentoPDF:null
    })

    useEffect(() => {
      if ( data.length === 0 ) getRegistrosOrdenesCompra()
      if ( tipoVehiculos.length === 0 ) getTipoVehiculos()
      if ( nombreDeClientes.length === 0 ) getNombreDeClientes()
    }, [])
    
    const getTipoVehiculos = async () => {
        url = ApiUrl + "api/get_tipovehiculos"
        const tipos = await axiosPostService( url, agencia )
        setTipoVehiculos(tipos)
        // console.log('tipos',tipos);
    }

    const getNombreDeClientes = async () => {
        url = ApiUrl + "api/clientes"
        const clients = await axiosPostService( url, agencia )
        setNombreDeClientes(clients)
    }

    const getRegistrosOrdenesCompra = async() => {
        url = ApiUrl + "api/get_orden_compra"
        const buyorders = await axiosPostService( url, agencia )
        try {
            dataTableDestroy()
            setData(buyorders)
            dataTable()
            
        } catch (error) {
            toast.error("Error al cargar registros en tabla.")
        }
    }

    const setValuesWrittenForm = ( ordenCompraObject ) => {
        isEditMode
        ?
        setOrdenDeCompra({
            Cliente:ordenDeCompra.Cliente,
            Ubicacion:ordenDeCompra.Ubicacion,
            OrdenCompra:ordenDeCompra.OrdenCompra,
            Cantidad:ordenDeCompra.Cantidad,
            TipoVehiculo:ordenDeCompra.TipoVehiculo,
            Existencia:ordenDeCompra.Existencia,
            DocumentoPDF:ordenDeCompra.DocumentoPDF,
            Id:ordenDeCompra.Id
        })
        :
        setOrdenDeCompra({
            Cliente:ordenCompraObject.Cliente,
            Ubicacion:ordenCompraObject.Ubicacion,
            OrdenCompra:ordenCompraObject.OrdenCompra,
            Cantidad:ordenCompraObject.Cantidad,
            TipoVehiculo:ordenCompraObject.TipoVehiculo,
            Existencia:ordenCompraObject.Existencia,
            DocumentoPDF:ordenCompraObject.DocumentoPDF,
        })
    }

    const nuevoRegistroOrdenCompra = ( ordenCompraObject ) => {

        if ( ordenCompraObject.OrdenCompra === "" ){
            toast.info('El campo Orden de Compra se encuentra vacío.')
            setValuesWrittenForm( ordenCompraObject )
            return;
        }
        if ( ordenCompraObject.Cantidad === "" ){
            toast.info('El campo Cantidad se encuentra vacío.')   
            setValuesWrittenForm( ordenCompraObject )          
            return;
        }
        if ( ordenCompraObject.Existencia === "" ){
            toast.info('El campo Existencia se encuentra vacío.')
            setValuesWrittenForm( ordenCompraObject )
            return;
        }

        if ( isEditMode ) {
            if ( ordenCompraObject.DocumentoPDF !== "1" && ordenCompraObject.DocumentoPDF !== "" ) {
                if ( ordenCompraObject.DocumentoPDF.type !== "application/pdf" ) {
                    toast.info('El archivo seleccionado no es un formato válido, Favor de seleccionar formato PDF.')
                    setValuesWrittenForm( ordenCompraObject )
                    return;
                }
            }
        }
        if ( !isEditMode ) {
            if ( ordenCompraObject.DocumentoPDF !== null) {
                    if ( ordenCompraObject.DocumentoPDF.type !== "application/pdf" ) {
                        toast.info('El archivo seleccionado no es un formato válido, Favor de seleccionar formato PDF.')
                        setValuesWrittenForm( ordenCompraObject )
                        return;
                    }
            }

        }

        if ( isEditMode ) {
            updateOrderSelected(ordenCompraObject)
            return;
        }

        if ( !isEditMode ) {
            createOrderWritten(ordenCompraObject)
        }

        /* console.log('another module', ordenCompraObject)
        resetValues();
        closeModal(); 
        resetValues();
        
        */
    }

    const createOrderWritten = async ( ordenCompraObject ) => {
        url = `${ApiUrl}api/create_orden_compra`
        const formData = new FormData();
        formData.append('file', ordenCompraObject.DocumentoPDF)
        formData.append('agencia', JSON.stringify(agencia))
        formData.append('body', JSON.stringify({
            Cliente:ordenCompraObject.Cliente,
            Ubicacion:ordenCompraObject.Ubicacion,
            OrdenCompra:ordenCompraObject.OrdenCompra,
            Cantidad:ordenCompraObject.Cantidad,
            TipoVehiculo:ordenCompraObject.TipoVehiculo,
            Existencia:ordenCompraObject.Existencia    
        }))

        await axios.post(url, formData, {
            headers: {
                'content-type': 'multipart/form-data'
            }
        })
        .then(response => {
            if ( response['data'].isCreated ) {
                getRegistrosOrdenesCompra(); //order by ??
                toast.success(RegistroExitoso);
                closeModal();

            }
        })
        .catch(err => {
            toast.error(ErrorConexion)
            console.log(err);
        })
        
    }

    const updateOrderSelected = async ( ordenCompraObject ) => {
        url = `${ApiUrl}api/update_orden_compra`
        const formData = new FormData();
        formData.append('file', ordenCompraObject.DocumentoPDF)
        formData.append('agencia', JSON.stringify(agencia))
        formData.append('body', JSON.stringify({
            Cliente:ordenCompraObject.Cliente,
            Ubicacion:ordenCompraObject.Ubicacion,
            OrdenCompra:ordenCompraObject.OrdenCompra,
            Cantidad:ordenCompraObject.Cantidad,
            TipoVehiculo:ordenCompraObject.TipoVehiculo,
            Existencia:ordenCompraObject.Existencia,
            Id:ordenCompraObject.Id,
                
        }))

        await axios.patch(url, formData, {
            headers: {
                'content-type': 'multipart/form-data'
            }
        })
        .then(response => {
            console.log('isupdated',response)
            if ( response['data'].isUpdated ) {
                getRegistrosOrdenesCompra()
                setisEditMode(false)
                toast.success(RegistroExitoso)
                closeModal()
            }
            /* 
                getClientes();
                setisEditMode(false);
                toast.success(RegistroExitoso);
                closeModal();
            */

        })
        .catch(err => {
            toast.error(ErrorConexion)
            console.log(err);
        })
    }

    const resetValues = () => {
        setOrdenDeCompra({
            Cliente:"",
            Ubicacion:"",
            OrdenCompra:"",
            Cantidad:"",
            TipoVehiculo:"",
            Existencia:"",
            DocumentoPDF:null
        })
    }

    const createOrdenCompra = () => {
        setisEditMode( false );
        setOrdenDeCompra({...ordenDeCompra});
        openModal();
    }

    const downloadPDF = async (Id) => {
        url = ApiUrl + "api/send_pdf"

        let body = {Id, agencia}
        await axios.post(url, body, {responseType:'blob'})
        .then(response => {
            const fileUrl = window.URL.createObjectURL(response['data']);
            window.open(fileUrl, '_blank');
            /*  
            saveAs(response['data'],"filepdf" + ".pdf") ***imprimir con librería
            var link = document.createElement('a'); link.href=window.URL.createObjectURL(response['data']); link.download="mufile.pdf"; link.click();  ***imprimir objeto nativo de javascript
            */
        })
        .catch(err => {
            console.log(err);
        })
    }

    const editOrder = (order_compra) => {
        console.log('edit row',order_compra);
        setisEditMode(true);
        setOrdenDeCompra({
            Cliente         : order_compra.Cliente,
            Ubicacion       : order_compra.Ubicacion,
            OrdenCompra     : order_compra.OrdenCompra,
            Cantidad        : order_compra.Cantidad,
            TipoVehiculo    : order_compra.TipoVehiculo,
            Existencia      : order_compra.Existencia,
            DocumentoPDF    : order_compra.DocumentoPDF,
            Id              : order_compra.Id
        })

        openModal()
    }

    const dataTable = () =>{
        $('#ordenDeCompra').DataTable(ordenDeCompraDataTable);
    }
    
    const dataTableDestroy = () =>{
        $('#ordenDeCompra').DataTable().destroy();
    }

  return (
    <div className='flex-container ml-2 mr-2'>
        <Modal isOpen={isOpenModal} closeModal={closeModal}>
            <ModalOrdenDeCompra
            tipoVehiculos={tipoVehiculos}
            nombreDeClientes={nombreDeClientes}
            data={ordenDeCompra}
            onSubmit={nuevoRegistroOrdenCompra}
            editMode={isEditMode}
            />
        </Modal>
        <div className='flex-item'>
            <button 
            type='button' 
            className='btn btn-info'
            onClick={createOrdenCompra}
            >Registrar</button>
            
        </div>
        {
        data.length === 0 &&
        <div className='flex-item'>
            <strong>Cargando...</strong>
            <div className="spinner-border" role="status" aria-hidden="true"></div>
        </div>
        }
        <div className='flex-item'>
            <table id='ordenDeCompra' className='table table-bordered table-striped compact'>{/* display compact */} 
                <thead>
                    <tr>
                        <th className='text-center'>Cliente</th>
                        <th className='text-center'>Ubicación</th>
                        <th className='text-center'>Orden Compra</th>
                        <th className='text-center'>Cantidad</th>
                        <th className='text-center'>Tipo Vehículo</th>
                        <th className='text-center'>Existencia</th>
                        <th className='text-center'>Documento PDF</th>
                        <th className='text-center'>Editar</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        data.length > 0 ?
                        data.map(order => {
                            return (
                                <tr>
                                    <td className='text-center'>{order.Cliente}</td>
                                    <td className='text-center'>{order.Ubicacion}</td>
                                    <td className='text-center'>{order.OrdenCompra}</td>
                                    <td className='text-center'>{order.Cantidad}</td>
                                    <td className='text-center'>{order.TipoVehiculo}</td>
                                    <td className='text-center'>{order.Existencia}</td>
                                    <td className='text-center'>{
                                    order.DocumentoPDF === "1" 
                                    ? 
                                    <button
                                        title='Descargar PDF'
                                        type='button'
                                        className='btn btn-outline-danger'
                                        onClick={() => downloadPDF(order.Id)}
                                    ><FontAwesomeIcon icon={faFilePdf}/></button> 
                                    : 
                                    ""
                                    }</td>
                                    <td className='text-center'>
                                        <button
                                        title='Editar orden de compra'
                                        type='button'
                                        className='btn btn-outline-dark'
                                        onClick={() => editOrder(order)}
                                        >
                                            <FontAwesomeIcon icon={faEdit}/>
                                        </button>
                                    </td>
                                </tr>
                            )
                        })
                        :
                        ""   
                    }
                </tbody>
            </table>
        </div>
    </div>
  )
}

export default OrdenDeCompra1


{/* <tr>
    <div class="d-flex align-items-center">
    <strong>Cargando...</strong>
    <div className="spinner-border ml-auto" role="status" aria-hidden="true"></div>
    </div>
</tr> */}