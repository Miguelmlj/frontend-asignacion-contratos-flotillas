import React,{ useState } from 'react'

import { ToastContainer, toast } from 'react-toastify';
import { getAgencia } from '../../helpers/getAgencia';
import OrdenDeCompra1 from './componentes/OrdenDeCompra1';
import '../../css/ordenDeCompra/ordenDeCompra.css'
import "datatables.net-dt/css/jquery.dataTables.min.css"

const OrdenDeCompra = () => {
  const ordenDeCompra = 1;
  const segundaPestana = 2;
  const terceraPestana = 3;
  const agencia = getAgencia();
  let url = '';
  const [componenteAsignacion, setComponenteAsignacion] = useState(1)

  const onChange = ( value ) => {
    setComponenteAsignacion( value );
  }

  return (
    <div className='content-wrapper'>
      <div className='content-header'>
        <div className='container-fluid'>
          <div className='row'>
            <div className='col-sm-12'>
              <div className='card card-outline card-primary'>
                <div className='card-header'>
                  <h5 className='m-0 text-dark'>ORDEN DE COMPRA</h5>
                </div>
              </div>    
            </div>  
          </div>
        </div>  
      </div>
      <div className='container-fluid'>
        <div className='row ml-2 mr-2'>

          <ul className="nav nav-tabs mb-3">
            <li className="nav-item" role="presentation" onClick={() => onChange(ordenDeCompra)}>
              <a className={componenteAsignacion === 1 ? "nav-link handpointer active background-tabs" : "nav-link handpointer"}>
                <i className="fas fa-tasks"/>
                <small className='ml-2'>ORDEN DE COMPRA</small>
              </a>
            </li>
            <li className="nav-item" role="presentation" onClick={() => onChange(segundaPestana)}>
              <a className={componenteAsignacion === 2 ? "nav-link handpointer active background-tabs" : "nav-link handpointer"}>
                <i className="fas fa-list-ol"/>
                <small className='ml-2'>2 PESTAÑA</small>
              </a>
            </li>
            <li className="nav-item" role="presentation" onClick={() => onChange(terceraPestana)}>
              <a className={componenteAsignacion === 3 ? "nav-link handpointer active background-tabs" : "nav-link handpointer"}>
                <i className="fas fa-check-double"/>
                <small className='ml-2'>3 PESTAÑA</small>
              </a>
            </li>
            {/* <li className="nav-item" role="presentation" onClick={() => onChange(resumenDeContratos)}>
              <a className={componenteAsignacion === 4 ? "nav-link handpointer active background-tabs" : "nav-link handpointer"}>
                <i className="far fa-list-alt"/>
                <small className='ml-2'>RESUMEN DE CONTRATOS</small>
              </a>
            </li> */}
          </ul>
          
        </div>
        
        {
            componenteAsignacion === 1 ?  <OrdenDeCompra1 agencia={agencia}/> :
            componenteAsignacion === 2 ?  <div>"modulo en construcción"</div> :
            componenteAsignacion === 3 &&  <div>"modulo en construcción"</div> 
            // : componenteAsignacion === 4 && <ResumenDeContratos agencia={agencia} clientes={clientes}/>
        }
        
      </div>
      <ToastContainer />
    </div>
  )
}

export default OrdenDeCompra