import React, { useState, useEffect } from 'react'

import { TablaPermisoDesvio } from './tablas-vistas/TablaPermisoDesvio';
import { axiosPostService } from '../../../services/asignacionLoteService/AsignacionLoteService'
import { ApiUrl } from '../../../services/ApiRest'
import { toast } from 'react-toastify';
import { validarFecha } from '../../../helpers/fecha';


const DatosDPP1 = ({ clientes, agencia }) => {

  const [writtendata, setWrittendata] = useState({
    Cliente               : clientes.length > 0 ? `${clientes[0].Nombre_corto}` : "",
    NumeroCliente         : clientes.length > 0 ? `${clientes[0].Num_cliente}`  : 0,
    UbicacionCliente      : clientes.length > 0 ? `${clientes[0].Ubicacion}`    : "",
    PermisoDesvio         : "DPP",
    FolioDesvio           : "",
    FechaSalida           : "",
    FechaLlegada          : "",
    FechaEntrega          : "",
    FechaVencimiento      : "",
    FechaVencimientoDPP1  : "",
    FolioDPP              : ""
    //considerar una propiedad Id del Cliente (tabla clientes_flotillas)
  })

  const [isPreviewTable, setIsPreviewTable]                                 = useState(false)
  const [VINClientes, setVINClientes]                                       = useState([])
  const [VINClientesGenerados, setVINClientesGenerados]                     = useState([])
  const [updateVentasFlotillasDMSTable, setUpdateVentasFlotillasDMSTable]   = useState(false)
  const [VINSGeneratedinBD, setVINSGeneratedinBD] = useState(false)
  const emptySpace = ""
  const DPP = "DPP"
  const Contado = "Contado"
  let url = '';

  useEffect(() => {
    updateTablaVentasFlotillasDMS()
    if ( clientes.length > 0 ) setWrittendata({...writtendata, Cliente: clientes[0].Nombre_corto, NumeroCliente: clientes[0].Num_cliente, UbicacionCliente: clientes[0].Ubicacion})
  }, [clientes])

  //El endpoint llamado en esta función se encuentr en las rutas de [asignacion_lote] backend
  const updateTablaVentasFlotillasDMS = async () => {
    /* if ( clientes.length > 0 ) {
      url = ApiUrl + "api/updateregistrosoracle"
      const body =  { agencia: agencia }
      const response = await axiosPostService( url, body )
      if ( response.isUpdated ) {
          // setUpdateVentasFlotillasDMSTable( true )
          // getNombresLoteCliente( clientes[0] )
          toast.info("tablaflotillasdms")

      }
    } */
    setUpdateVentasFlotillasDMSTable(true)
    getVINSCliente(clientes[0].Num_cliente);
  }

  const getVINSCliente = async ( NumCliente ) => {
    url = ApiUrl + "api/dpp_contado/getvinscliente";
    const body_cliente = { Agencia: agencia, NumCliente: NumCliente };
    let total_vines_cliente = await axiosPostService( url, body_cliente);
    
    if ( VINClientesGenerados.length > 0 && !isPreviewTable) setVINClientesGenerados([]);
    if ( total_vines_cliente.length  > 0 ) total_vines_cliente = agregarVariableIsSelected(total_vines_cliente);
    setVINClientes(total_vines_cliente);

  }

  const agregarVariableIsSelected = (total_vines_cliente) => {
    const list = total_vines_cliente.map((obj) => {
      let changeObj = {
        ...obj,
        isVinSelected : obj.PermisoDesvio !== null ? true : false,
        isDisabled    : obj.PermisoDesvio !== null ? true : false,
        isOnBD        : obj.PermisoDesvio !== null ? true : false,
      }
      return changeObj;
    })

    const listOrdered =  ordenarVinesDeshabilitadosAlFinal( list )
    return listOrdered;
  }

  const ordenarVinesDeshabilitadosAlFinal = ( list ) => {
    const mixedLists = [
       ...list.filter((row) => { return row.PermisoDesvio === null }), //listVinsHabilitados
       ...list.filter((row) => { return row.PermisoDesvio !== null })  //listVinsDeshabilitados
      ]
    return mixedLists;
  }

  const changeSelectClientes = async(e) => {
    const [ Ubicacion, Nombre_cliente, Num_cliente ] = e.target.value.split("|");
    getVINSCliente(Num_cliente)
    setWrittendata({
      ...writtendata,
      Cliente          : Nombre_cliente,
      NumeroCliente    : Num_cliente,
      UbicacionCliente : Ubicacion
    })
    setVINSGeneratedinBD(false)
  }

  const OnChange = (e) => {

    if ( e.target.name === "Cliente" ) {
      const [ ubi, nom, num ] = e.target.value.split("|");
      setWrittendata({
        ...writtendata,
        Cliente          : nom,
        NumeroCliente    : num,
        UbicacionCliente : ubi
      })
      return;
    }

    if ( e.target.name === "PermisoDesvio" ) {
      setWrittendata({
        ...writtendata,
        [e.target.name] : e.target.value,
        FolioDPP        : ""
      })

      return;
    }

    setWrittendata({
      ...writtendata,
      [e.target.name]: e.target.value
    })

  }

  const handleVinSelected = ( e, registro ) => {
    const checked = e.target.checked;
    const updateVIN = VINClientes.map((row) => {
      if ( registro.Vin === row.Vin ) {
        let updateRow = {
          ...row,
          isVinSelected         : checked,
          FechaEntrega          : !checked ? "" : writtendata.FechaEntrega,
          FechaLlegada          : !checked ? "" : writtendata.FechaLlegada,
          FechaSalida           : !checked ? "" : writtendata.FechaSalida,
          FechaVencimiento      : !checked ? "" : writtendata.FechaVencimiento,
          FechaVencimientoDPP1  : !checked ? "" : writtendata.FechaVencimientoDPP1,
          FolioDPP              : !checked ? "" : writtendata.FolioDPP,
          FolioDesvio           : !checked ? "" : writtendata.FolioDesvio,
          PermisoDesvio         : !checked ? "" : writtendata.PermisoDesvio,
          // FechaSolicitudFase2   : !checked ? "" : writtendata.FechaEntrega,
          // FechaVencimientoFase2 : !checked ? "" : writtendata.FechaEntrega,
        }
        insertNewVIN(updateRow, checked)
        return updateRow;
      }

      return row;
    })

    setVINSGeneratedinBD(false);
    setVINClientes(updateVIN)
  }

  const insertNewVIN = (object, checked) => {
    if ( !checked ) {
      const updateList = VINClientesGenerados.filter((row) => {
          return row.Vin !== object.Vin
      })

      setVINClientesGenerados(updateList)
      return;
    }

    setVINClientesGenerados([
      ...VINClientesGenerados,
      {
        ...object,
        Cliente           : writtendata.Cliente,
        NumeroCliente     : writtendata.NumeroCliente,
        UbicacionCliente  : writtendata.UbicacionCliente
      }
    ])
  }

  const handleGuardarPermisoDesvio = () => {
    setVINSGeneratedinBD(true)
    getVINSCliente(writtendata.NumeroCliente)

  }

  const onGenerateTable = () => {

    if ( existsAnyEmptyDate() !== undefined) {
      toast.info("Existen vins seleccionados sin fecha asignada.")
      return;
    }

    if ( existsAnyEmptyFolioDesvio() !== undefined ) {
      toast.info("Existen vins seleccionados sin folio desvío asignado.")
      return;
    }

    if ( writtendata.PermisoDesvio ===  DPP && existsAnyEmptyFolioDPP() !== undefined) {
      toast.info("Existen vins seleccionados sin folio dpp asignado.")
      return;
    }

    if ( VINSGeneratedinBD ) setVINClientesGenerados([]);
    setIsPreviewTable(!isPreviewTable)
  }

  const existsAnyEmptyDate = () => {
    return VINClientesGenerados.find( (obj) => {
      if ( 
           obj.FechaEntrega     === emptySpace || 
           obj.FechaLlegada     === emptySpace || 
           obj.FechaSalida      === emptySpace || 
           obj.FechaVencimiento === emptySpace ||
           ( writtendata.PermisoDesvio ===  DPP && obj.FechaVencimientoDPP1 === emptySpace )
         ) return obj;
    })
  }

  const existsAnyEmptyFolioDesvio = () => { 
    return VINClientesGenerados.find( (obj) => {
      if ( obj.FolioDesvio === emptySpace ) return obj;
    })
  }
  
  const existsAnyEmptyFolioDPP = () => { 
    return VINClientesGenerados.find( (obj) => {
      if ( obj.FolioDPP === emptySpace ) return obj;
    })
  }

  const OnBlur = (e) => {
    const inputFolioDesvio = e.target.value;
    const updateVINS = VINClientes.map((obj) => {
        return {
          ...obj,
          isDisabled : obj.isOnBD ? obj.FolioDesvio !== inputFolioDesvio  ? true : false : false
        }
    })

    setVINClientes( updateVINS );
  }

  return (
    <>
      <div className="row m-2">
        <div className="col-6">
          <div className="row d-flex justify-content-between pl-2 pr-4">
              <h6 className='mr-4'>Seleccionar Cliente: </h6>
              <select 
              className='form-select select-class-1' 
              onChange={(e) => changeSelectClientes(e)} 
              disabled={!updateVentasFlotillasDMSTable || isPreviewTable}
              tabIndex={1}
              >
                {
                clientes
                .map(cliente => {
                  return (
                    <option 
                    value={`${cliente.Ubicacion}|${cliente.Nombre_corto}|${cliente.Num_cliente}`} 
                    >
                        {`${cliente.Nombre_corto} ${cliente.Ubicacion}`}
                    </option>
                  )
                })
                }
              </select>
          </div>
        </div>
        <div className="col-6">
          <div className="row d-flex justify-content-between pl-2 pr-4">
              <h6 className='mr-4'>Permiso Desvío: </h6>
              <select 
              name='PermisoDesvio' 
              className='form-select select-class-1' 
              tabIndex={2} 
              onChange={OnChange}
              disabled={!updateVentasFlotillasDMSTable || isPreviewTable}
              >
                    <option value="DPP"> DPP </option>
                    <option value="Contado"> Contado </option>
              </select>
          </div>
        </div>
      </div>
      <div className="row m-2">
        <div className="col-6" >
          <div className="row d-flex justify-content-between pl-2 pr-4">
              <h6 className='mr-4'>Folio Desvío: </h6>
              <input 
              className='input-class' 
              type="text" 
              name="FolioDesvio" 
              value={writtendata.FolioDesvio} 
              onChange={OnChange}
              onBlur={OnBlur}
              disabled={!updateVentasFlotillasDMSTable || isPreviewTable}
              tabIndex={3}
              />
          </div>
        </div>
        <div className="col-6" >
          <div className="row d-flex justify-content-between pl-2 pr-4">
              <h6 className='mr-4'>Fecha Salida: </h6>
              <input 
              className='input-class' 
              type="date" 
              name="FechaSalida" 
              value={writtendata.FechaSalida} 
              onChange={OnChange}
              disabled={!updateVentasFlotillasDMSTable || isPreviewTable}
              tabIndex={4}
              min="2022-01-01"
              />
              
          </div>
        </div>
      </div>
      <div className="row m-2">
        <div className="col-6" >
          <div className="row d-flex justify-content-between pl-2 pr-4">
              <h6 className='mr-4'>Fecha Llegada: </h6>
              <input 
              className='input-class' 
              type="date" 
              name="FechaLlegada" 
              value={writtendata.FechaLlegada} 
              onChange={OnChange}
              disabled={!updateVentasFlotillasDMSTable || isPreviewTable}
              tabIndex={5}
              min="2022-01-01"
              />
          </div>
        </div>
        <div className="col-6" >
          <div className="row d-flex justify-content-between pl-2 pr-4">
              <h6 className='mr-4'>Fecha Entrega: </h6>
              <input 
              className='input-class' 
              type="date" 
              name="FechaEntrega" 
              value={writtendata.FechaEntrega} 
              onChange={OnChange}
              disabled={!updateVentasFlotillasDMSTable || isPreviewTable}
              tabIndex={6}
              min="2022-01-01"
              />
          </div>
        </div>
      </div>
      <div className="row m-2">
        <div className="col-6" >
          <div className="row d-flex justify-content-between pl-2 pr-4">
              <h6 className='mr-4'>Fecha Vencimiento Folio Desvío: </h6>
              <input 
              className='input-class' 
              type="date" 
              name="FechaVencimiento" 
              value={writtendata.FechaVencimiento} 
              onChange={OnChange}
              disabled={!updateVentasFlotillasDMSTable || isPreviewTable}
              tabIndex={7}
              min="2022-01-01"
              />
          
          </div>
        </div>
        {
          writtendata.PermisoDesvio === "DPP" &&
          <div className="col-6" >
          <div className="row d-flex justify-content-between pl-2 pr-4">
              <h6 className='mr-4'>Fecha Vencimiento DPP: </h6>
              <input 
              className='input-class' 
              type="date" 
              name="FechaVencimientoDPP1" 
              value={writtendata.FechaVencimientoDPP1} 
              onChange={OnChange}
              min="2022-01-01"
              tabIndex={8}
              disabled={!updateVentasFlotillasDMSTable || isPreviewTable}
              />
          </div>
          <div className="row d-flex justify-content-between pl-2 pr-4">
              <h6 className='mr-4'>Folio DPP: </h6>
              <input 
              className='input-class' 
              type="text" 
              name="FolioDPP" 
              value={writtendata.FolioDPP} 
              onChange={OnChange}
              tabIndex={8}
              disabled={!updateVentasFlotillasDMSTable || isPreviewTable}
              />
          </div>
        </div>
        }
      </div>


      <div className="row m-2 d-flex justify-content-between">
        <h6 className='ml-2'>Seleccionar VIN's que integran el permiso de desvío</h6>
        <button 
        type='button' 
        className='btn btn-info mt-2 mb-2' 
        onClick={onGenerateTable}
        disabled={VINClientesGenerados.length === 0}
        >
          { isPreviewTable ? 'Regresar' : 'Vista Previa'}
        </button>
      </div>
      {
        !isPreviewTable
        && 
        <div className="row m-2">
          <table className='table display compact'>
            <thead style={{backgroundColor:'#1565C0', color:'white'}}>
              <tr className='text-center'>
                <th>VIN</th>
                <th>Seleccionar VIN</th>
                <th>Permiso Desvio</th>
                <th>Folio Desvio</th>
                <th>Fecha Salida</th>
                <th>Fecha Llegada</th>
                <th>Fecha Entrega</th>
                <th>Fecha Vencimiento</th>
                {writtendata.PermisoDesvio === "DPP" && <><th>Folio DPP</th><th>Fecha Vencimiento DPP</th></>}
              </tr>
            </thead>
            <tbody style={{backgroundColor:'#FFFFE0'}}>{/* #FFFACD */}
            {
              VINClientes.length 
              > 0 ?
              VINClientes
              .map((registro) => {
                return (
                  <tr className='text-center'>
                    <td>{registro.Vin}</td>
                    <td>
                      <input 
                      checked={registro.isVinSelected} 
                      className="form-check-input" 
                      name="vin_selected" 
                      onChange={( e ) => handleVinSelected( e, registro )}
                      type="checkbox" 
                      value="vin_selected"
                      disabled={ registro.isDisabled } 
                      /></td>
                    <td>{registro.PermisoDesvio}</td>
                    <td>{registro.FolioDesvio}</td>
                    <td>{validarFecha(registro.FechaSalida)}</td>
                    <td>{validarFecha(registro.FechaLlegada)}</td>
                    <td>{validarFecha(registro.FechaEntrega)}</td>
                    <td>{validarFecha(registro.FechaVencimiento)}</td>
                    {writtendata.PermisoDesvio === "DPP" && 
                    <>
                    <td>{registro.FolioDPP}</td>
                    <td>{validarFecha(registro.FechaVencimientoDPP1)}</td>
                    </>}
                  </tr>
                )
              })
              :
              <tr className='p-2'>
                  <td>No existen registros para el cliente seleccionado.</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  {writtendata.PermisoDesvio === "DPP" && <><td></td><td></td></>}
              </tr>
            }
            </tbody>
          </table>
      </div>
      }

      <TablaPermisoDesvio
        agencia={agencia}
        data={VINClientesGenerados}
        isPreviewTable={isPreviewTable}
        handleGuardarPermisoDesvio={handleGuardarPermisoDesvio}
        valorPermisoDesvio={writtendata.PermisoDesvio}
        nombreCliente={writtendata.Cliente}
      />
      
      <div className="row m-2">
        <button 
        type='button' 
        className='btn btn-info mt-2 mb-2' 
        onClick={onGenerateTable} 
        disabled={VINClientesGenerados.length === 0}
        >
          { isPreviewTable ? 'Regresar' : 'Vista Previa'}
        </button>
      </div>
      
    </>
  )
}

export default DatosDPP1