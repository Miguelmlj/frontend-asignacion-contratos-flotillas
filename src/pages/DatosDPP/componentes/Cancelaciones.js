import React, { useState, useEffect } from 'react'

import swal from 'sweetalert'
import { toast } from 'react-toastify'

import { ApiUrl } from '../../../services/ApiRest'
import { axiosPatchService, axiosPostService } from '../../../services/asignacionLoteService/AsignacionLoteService'
import { validarFecha } from '../../../helpers/fecha'
import TablaCancelaciones from './tablas-vistas/TablaCancelaciones'

const Cancelaciones = ({
  agencia,
  clientes
}) => {

  const permisoDesvioInicial = "DPP"
  const DPP = 'DPP'
  const Contado = 'Contado'
  const [writtendata, setWrittendata] = useState({
    NumeroCliente     : clientes.length > 0 ? `${clientes[0].Num_cliente}`  : 0,
    PermisoDesvio     : permisoDesvioInicial,
    FolioDesvio       : "",
  })
  const [VINClientes, setVINClientes] = useState([])
  const [VINClientesGenerados, setVINClientesGenerados] = useState([])
  const [foliosDesvioList, setFoliosDesvioList] = useState([])
  const [VINSGeneratedinBD, setVINSGeneratedinBD] = useState(false)
  const [isPreviewTable, setIsPreviewTable] = useState(false)
  const [table, setTable] = useState(permisoDesvioInicial)
  let url = '';

  useEffect(() => {
    if ( clientes.length > 0 ) {
      setWrittendata({
        ...writtendata, 
        NumeroCliente     : clientes[0].Num_cliente, 
      })
      getFoliosDesvioByClienteAndPermiso( clientes[0].Num_cliente, writtendata.PermisoDesvio )
    }
  }, [clientes])

  const getFoliosDesvioByClienteAndPermiso = async ( ncliente, permisoDesvio ) => { 
    url = ApiUrl + "api/dpp_contado/getfoliosdesviobycliente"
    const body_cliente = { Agencia: agencia, NumCliente: ncliente, permisoDesvio:permisoDesvio, pestana: "Cancelaciones" };
    let total_folios_desvio = await axiosPostService( url, body_cliente );
    setFoliosDesvioList(total_folios_desvio)

    if ( total_folios_desvio.length > 0 ) {
      let firstFolioOfList = total_folios_desvio[0].FolioDesvio;
      getVinsByFolioDesvio( firstFolioOfList, ncliente, permisoDesvio )
      setWrittendata({ ...writtendata, NumeroCliente: ncliente, FolioDesvio: firstFolioOfList, PermisoDesvio: permisoDesvio })
    }

    if ( total_folios_desvio.length === 0 ) {
      setVINClientes([])
      if ( !isPreviewTable && VINClientesGenerados.length > 0 ) setVINClientesGenerados([]);
      setWrittendata({ ...writtendata, NumeroCliente: ncliente, FolioDesvio: "", PermisoDesvio: permisoDesvio })
    }

  }

  const getVinsByFolioDesvio = async ( folioDesvio, NumCliente, permisoDesvio ) => {
    url = ApiUrl + "api/dpp_contado/getvinsclienttocancel"
    const body_cliente = { 
      Agencia: agencia, 
      NumCliente: NumCliente, 
      PermisoDesvio: permisoDesvio,
      folioDesvio   : folioDesvio  
    };

    if ( !isPreviewTable && VINClientesGenerados.length > 0 ) setVINClientesGenerados([]);
    let total_vins_to_cancel = await axiosPostService( url, body_cliente )
console.log('total_vins_to_cancel', total_vins_to_cancel);
    if ( total_vins_to_cancel.length  > 0 ) total_vins_to_cancel = agregarVariableIsSelected(total_vins_to_cancel);
    setVINClientes(total_vins_to_cancel);

  }

  const insertNewVIN = (object, checked) => {
    if ( !checked ) {
      const updateList = VINClientesGenerados.filter((row) => {
          return row.VIN !== object.VIN
      })

      setVINClientesGenerados(updateList)
      return;
    }

    setVINClientesGenerados([
      ...VINClientesGenerados,
      {
        ...object,
        NumeroCliente     : writtendata.NumeroCliente,
      }
    ])
  }

  const agregarVariableIsSelected = ( total_vins_to_cancel ) => {
    const lista = total_vins_to_cancel.map((obj) => {
      let changeObj = {
        ...obj,
        isVinSelected: false
      }
      return changeObj;
    })
    return lista;
  }

  const handleVinSelected = ( e, registro ) => {
    const checked = e.target.checked;
    const updateVIN = VINClientes.map((row) => {
      if ( registro.VIN === row.VIN ) {
        let updateRow = {
          ...row,
          isVinSelected : checked,
        }
        insertNewVIN(updateRow, checked)
        return updateRow;
      }

      return row;
    })

    if ( VINSGeneratedinBD ) setVINSGeneratedinBD( false )
    setVINClientes( updateVIN )
  }

  const OnChange = (e) => {
    //PermisoDesvio
    if ( e.target.name === 'PermisoDesvio' ) {
      let permisodesv = e.target.value;
      setTable(permisodesv)
      getFoliosDesvioByClienteAndPermiso( writtendata.NumeroCliente, permisodesv )
      setVINSGeneratedinBD( false )
      return;
    }

    if ( e.target.name === 'FolioDesvio' ) {
      const [ foldesv, permdesv, numclient ] = e.target.value.split("|");
      getVinsByFolioDesvio( foldesv, writtendata.NumeroCliente, writtendata.PermisoDesvio )
      setWrittendata({ ...writtendata, FolioDesvio: foldesv })
      setVINSGeneratedinBD( false )
      return;
    }

  }

  const changeSelectClientes = async(e) => {
    const [ Ubicacion, Nombre_cliente, Num_cliente ] = e.target.value.split("|");
    getFoliosDesvioByClienteAndPermiso( Num_cliente, writtendata.PermisoDesvio );
    setVINSGeneratedinBD( false )
    
  }

  const afterCancel = () => {
    const numcli = writtendata.NumeroCliente;
    const permdesv = writtendata.PermisoDesvio;
    const foldesv = writtendata.FolioDesvio;
    setVINSGeneratedinBD( true );
    getVinsByFolioDesvio( foldesv, numcli, permdesv );
  }

  const onGenerateTable = () => {
    if ( VINSGeneratedinBD ) setVINClientesGenerados([]);
    setIsPreviewTable( !isPreviewTable )
  }

  return (
    <>
      <div className="row m-2">
        <div className="col-6">
          <div className="row d-flex justify-content-between pl-2 pr-4">
              <h6 className='mr-4'>Seleccionar Cliente: </h6>
              <select 
              // name='Cliente' 
              className='form-select select-class-1' 
              onChange={(e) => changeSelectClientes(e)}
              tabIndex={1}
              disabled={isPreviewTable}
              >
                {
                clientes
                .map(cliente => {
                  return (
                    <option 
                    value={`${cliente.Ubicacion}|${cliente.Nombre_corto}|${cliente.Num_cliente}`} 
                    >
                        {`${cliente.Nombre_corto} ${cliente.Ubicacion}`}
                    </option>
                  )
                })
                }
              </select>
          </div>
        </div>
      </div>
      <div className="row m-2">
        <div className="col-6">
            <div className="row d-flex justify-content-between pl-2 pr-4">
                <h6 className='mr-4'>Permiso Desvío: </h6>
                <select 
                name='PermisoDesvio' 
                className='form-select select-class-1' 
                tabIndex={2} 
                onChange={OnChange}
                disabled={isPreviewTable}
                >
                      <option value="DPP"> DPP </option>
                      <option value="Contado"> Contado </option>
                </select>
            </div>
        </div>
      </div>
      <div className="row m-2">
        <div className="col-6">
            <div className="row d-flex justify-content-between pl-2 pr-4">
                <h6 className='mr-4'>Folio Desvío: </h6>
                <select 
                name='FolioDesvio' 
                className='form-select select-class-1' 
                tabIndex={2} 
                onChange={OnChange}
                disabled={ foliosDesvioList.length === 0 || isPreviewTable}
                >
                    {
                      foliosDesvioList.map(foliod => {
                        return (
                          <option value={ `${foliod.FolioDesvio}|${foliod.PermisoDesvio}|${foliod.Cliente}` }>
                              { foliod.FolioDesvio }
                          </option>
                        )
                      })
                    }
              </select>
            </div>
        </div>
      </div>

      <div className="row m-2 d-flex justify-content-between">
        <h6 className='ml-2'>Seleccionar Permiso y Folio Desvío, que desea cancelar</h6>
        <button 
        type='button' 
        className='btn btn-info mt-2 mb-2' 
        // onClick={onCancelFolio}
        onClick={onGenerateTable}
        disabled={VINClientesGenerados.length === 0}
        >
          { isPreviewTable ? <small>Regresar</small> :  <small>Vista previa</small> }
        </button>
      </div>

        {
          !isPreviewTable &&
          table !== "DPP"
          ?
          <div className="row m-2">
          <div className='table-responsive'>
          <table className='table display compact'>
            <thead style={{backgroundColor:'#1565C0', color:'white'}}>
              <tr className='text-center'>
                <th>Seleccionar</th>
                <th>VIN</th>
                <th>Fecha Vencimiento</th>
              </tr>
            </thead>
            <tbody style={{backgroundColor:'#FFFFE0'}}>{/* #FFFACD */}
            {
              VINClientes.length 
              > 0 ?
              VINClientes
              .map((registro) => {
                return (
                  <tr className='text-center'>
                    <td>
                      <input 
                      checked={registro.isVinSelected} 
                      className="form-check-input" 
                      name="vin_selected" 
                      onChange={( e ) => handleVinSelected( e, registro )}
                      type="checkbox" 
                      value="vin_selected"
                      disabled={ '' /* disabledVINSelected(registro.vinBlocked) */ } 
                      />
                    </td>
                    <td>{registro.VIN}</td>
                    <td>{validarFecha(registro.FechaVencimiento)}</td>
                  </tr>
                )
              })
              :
              <tr className='p-2'>
                  <td>No existen registros para el cliente seleccionado.</td>
                  <td></td>
                  <td></td>
              </tr>
            }
            </tbody>
          </table>
          </div>
      </div>
        :
        !isPreviewTable &&
        <div className="row m-2">
          <div className='table-responsive'>
          <table className='table display compact'>
            <thead style={{backgroundColor:'#1565C0', color:'white'}}>
              <tr className='text-center'>
                <th>Seleccionar</th>
                {/* <th>Permiso Desvio</th> */}
                {/* <th>Folio Desvio</th> */}
                <th>VIN</th>
                <th>Folio DPP</th>
                <th>Fecha Vencimiento</th>
                <th>Fecha Vencimiento DPP 1</th>
              </tr>
            </thead>
            <tbody style={{backgroundColor:'#FFFFE0'}}>{/* #FFFACD */}
            {
              VINClientes.length 
              > 0 ?
              VINClientes
              .map((registro) => {
                return (
                  <tr className='text-center'>
                    
                    <td>
                      <input 
                      checked={registro.isVinSelected} 
                      className="form-check-input" 
                      name="vin_selected" 
                      onChange={( e ) => handleVinSelected( e, registro )}
                      type="checkbox" 
                      value="vin_selected"
                      disabled={ '' /* disabledVINSelected(registro.vinBlocked) */ } 
                      />
                    </td>
                    <td>{registro.VIN}</td>
                    <td>{registro.FolioDPP}</td>
                    <td>{validarFecha(registro.FechaVencimiento)}</td>
                    <td>{validarFecha(registro.FechaVencimientoDPP1)}</td>
                  </tr>
                )
              })
              :
              <tr className='p-2'>
                  <td>No existen registros para el cliente seleccionado.</td>
                  <td></td>
                  <td></td>
                  <td></td>
              </tr>
            }
            </tbody>
          </table>
          </div>
      </div>
      }

      <TablaCancelaciones
        agencia={agencia}
        data={VINClientesGenerados}
        isPreviewTable={isPreviewTable}
        valorPermisoDesvio={writtendata.PermisoDesvio}
        afterCancel={afterCancel}
      />


      <div className="row m-2">
        <button 
        type='button' 
        className='btn btn-info mt-2 mb-2' 
        // onClick={onCancelFolio} 
        onClick={onGenerateTable} 
        disabled={VINClientesGenerados.length === 0}
        >
          { isPreviewTable ? <small>Regresar</small> :  <small>Vista previa</small>}
        </button>
      </div>
      
    </>
  )
}

export default Cancelaciones