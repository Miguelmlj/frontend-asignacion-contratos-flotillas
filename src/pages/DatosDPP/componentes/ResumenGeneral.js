import React, { useState, useEffect } from 'react'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye } from '@fortawesome/free-solid-svg-icons';

import { axiosPostService } from '../../../services/asignacionLoteService/AsignacionLoteService';
import { ApiUrl } from '../../../services/ApiRest';
import { validarFecha } from '../../../helpers/fecha';

const ResumenGeneral = ({
  agencia,
  clientes
}) => {
  let url = '';
  const DPP = 'DPP'
  const Ambos = 'Ambos'
  const Todos = 'Todos'
  const Contado = 'Contado'
  const defaultDate = '1900-01-01'
  const permisoDesvioInicial = 'Ambos'
  const [handleSelects, setHandleSelects] = useState({
    NumeroCliente     : clientes.length > 0 ? `${clientes[0].Num_cliente}`  : 0,
    PermisoDesvio     : permisoDesvioInicial,
    FolioDesvio       : "",
  })
  const [data, setData] = useState([])
  const [foliosDesvioList, setFoliosDesvioList] = useState([])

  useEffect(() => {
    if ( clientes.length > 0 ) {
      setHandleSelects({
        ...handleSelects, 
        NumeroCliente     : clientes[0].Num_cliente, 
      })
      getFoliosDesvioByClienteAndPermiso( clientes[0].Num_cliente, handleSelects.PermisoDesvio )
    }
  }, [clientes])

  const getFoliosDesvioByClienteAndPermiso = async ( ncliente, permisoDesvio ) => { 
    url = ApiUrl + "api/dpp_contado/getfoliosdesviobycliente"
    const body_cliente = { Agencia: agencia, NumCliente: ncliente, permisoDesvio:permisoDesvio, pestana: "Resumen" };
    let total_folios_desvio = await axiosPostService( url, body_cliente );

    if ( total_folios_desvio.length === 0 ) {
      setData([])
      setFoliosDesvioList(total_folios_desvio)
      setHandleSelects({ ...handleSelects, NumeroCliente: ncliente, FolioDesvio: "", PermisoDesvio: permisoDesvio })
      console.log('total_folios_desvio',total_folios_desvio);
      return;
    }

    // total_folios_desvio.unshift({FolioDesvio:'Todos', PermisoDesvio:permisoDesvio, Cliente:ncliente})
    total_folios_desvio = [ {FolioDesvio:'Todos', PermisoDesvio:permisoDesvio, Cliente:ncliente}, ...total_folios_desvio ]
    setFoliosDesvioList([])
    setFoliosDesvioList(total_folios_desvio)

    const firstFolioOfList = total_folios_desvio[0].FolioDesvio;
    setHandleSelects({ ...handleSelects, NumeroCliente: ncliente, FolioDesvio: firstFolioOfList, PermisoDesvio: permisoDesvio })
    getVinsByFolioDesvio( firstFolioOfList, ncliente, permisoDesvio )
    console.log('total_folios_desvio',total_folios_desvio);

  }
  
  const getVinsByFolioDesvio = async ( folioDesvio, NumCliente, permisoDesvio ) => {
    url = ApiUrl + "api/dpp_contado/getvinsclienttoresumen"
    const body_cliente = { 
      Agencia: agencia, 
      NumCliente: NumCliente, 
      PermisoDesvio: permisoDesvio,
      folioDesvio   : folioDesvio  
    };
    const total_vins_to_show = await axiosPostService( url, body_cliente );
    console.log('total_vins_to_show', total_vins_to_show);
    setData(total_vins_to_show);
  }

  const changeSelectClientes =(e) => {}

  const showDetails = ( registro ) => { console.log('show details') }

  const OnChangeHandler = (e) => {
    const selectedSelect = e.target.name;
    if ( selectedSelect === 'PermisoDesvio' ) {
      // console.log('PermisoDesvio')
      const permisodesv = e.target.value;
      getFoliosDesvioByClienteAndPermiso( handleSelects.NumeroCliente, permisodesv )
      return;
    }
    if ( selectedSelect === 'FolioDesvio' ) {
      const [ foldesv, permdesv, numclient ] = e.target.value.split("|");
      getVinsByFolioDesvio( foldesv, handleSelects.NumeroCliente, handleSelects.PermisoDesvio )
      // console.log('FolioDesvio')
      return;
    }
    if ( selectedSelect === 'Cliente' ) {
      // console.log('Cliente')
      const [ Ubicacion, Nombre_cliente, Num_cliente ] = e.target.value.split("|");
      getFoliosDesvioByClienteAndPermiso( Num_cliente, handleSelects.PermisoDesvio );
    }
  }

  const showIcons =( registro ) => { 
    if ( registro.PermisoDesvio ===  DPP ) {
      if ( registro.FechaSolicitudFase2 !== null && registro.FechaVencimientoFase2 !== null ) return true;
      return false;
    }

    if ( registro.PermisoDesvio ===  Contado ) {
      if ( registro.numExtensiones > 0 ) return true;
      return false;
    }
  }

  return (
    <>
      <div className="row m-2">
        <div className="col-6">
          <div className="row d-flex justify-content-between pl-2 pr-4">
            <h6 className='mr-4'>Seleccionar Cliente: </h6>
            <select
              name='Cliente'
              className='form-select select-class-1'
              onChange={OnChangeHandler}
              // onChange={(e) => changeSelectClientes(e)}
              tabIndex={1}
              // disabled={isPreviewTable}
            >
              {
                clientes
                .map(cliente => {
                  return (
                    <option 
                    value={`${cliente.Ubicacion}|${cliente.Nombre_corto}|${cliente.Num_cliente}`} 
                    >
                        {`${cliente.Nombre_corto} ${cliente.Ubicacion}`}
                    </option>
                  )
                })
              }
            </select>
          </div>
        </div>
      </div>

      <div className="row m-2">
        <div className="col-6">
          <div className="row d-flex justify-content-between pl-2 pr-4">
            <h6 className='mr-4'>Permiso Desvío: </h6>
            <select
              name='PermisoDesvio'
              className='form-select select-class-1'
              tabIndex={2}
              onChange={OnChangeHandler}
            // disabled={isPreviewTable}
            >
              <option value="Ambos"> DPP Y CONTADO </option>
              <option value="DPP">     DPP FASE 1 - DPP FASE 2     </option>
              <option value="Contado"> CONTADO - EXTENSIÓN PERMISO </option>
            </select>
          </div>
        </div>
      </div>

      <div className="row m-2">
        <div className="col-6">
            <div className="row d-flex justify-content-between pl-2 pr-4">
              <h6 className='mr-4'>Folio Desvío: </h6>
              <select
                name='FolioDesvio'
                className='form-select select-class-1'
                tabIndex={2}
                onChange={OnChangeHandler}
                disabled={foliosDesvioList.length === 0 }
              >
                {
                  foliosDesvioList.map(foliod => {
                    return (
                      <option 
                      value={`${foliod.FolioDesvio}|${foliod.PermisoDesvio}|${foliod.Cliente}`}
                      selected={ foliod.FolioDesvio === Todos }
                      >
                        {foliod.FolioDesvio}
                      </option>
                    )
                  })
                }
              </select>
            </div>
          </div>
        </div>

        <div className="row m-2 d-flex justify-content-between">
          <h6 className='ml-2'>Total de registros encontrados:</h6>
        </div>

        <div className="row">
          <div className="table-responsive">
            <table className='table display compact'>
                <thead style={{backgroundColor:'#1565C0', color:'white'}}>
                  <tr className='text-center'>
                    <th>VIN</th>
                    <th>Permiso Desvío</th>
                    <th>Folio Desvío</th>
                    <th>Folio DPP</th>
                    <th>Fecha Vencimiento</th>
                    <th>Fecha Vencimiento DPP1</th>
                    <th>Folio DPP 2</th>
                    <th>Ver Detalles</th>
                  </tr>
                </thead>
                <tbody style={{backgroundColor:'#FFFFE0'}}>
                  {
                    data.length > 0 
                    ? data.map(( registro ) => {
                      return (
                        <tr className='text-center'>
                          <td>{registro.VIN}</td>
                          <td>{registro.PermisoDesvio}</td>
                          <td>{registro.FolioDesvio}</td>
                          <td>{registro.FolioDPP}</td>
                          <td>{validarFecha(registro.FechaVencimiento)}</td>
                          <td>{validarFecha(registro.FechaVencimientoDPP1)}</td>
                          <td>{registro.FolioDPP2}</td>
                          <td>
                            {showIcons( registro ) &&
                            <button
                            title='Ver las fechas de las extensiones || DPP'
                            type='button'
                            className='btn btn-outline-dark'
                            onClick={() => showDetails(registro)}
                            >
                              <FontAwesomeIcon icon={faEye}/>
                            </button>}
                          </td>
                        </tr>
                      )
                    })
                    :
                    <tr className='p-2'>
                      <td>No se encontraron registros</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  }
                </tbody>
            </table>
          </div>
        </div>
    </>
  )
}

export default ResumenGeneral