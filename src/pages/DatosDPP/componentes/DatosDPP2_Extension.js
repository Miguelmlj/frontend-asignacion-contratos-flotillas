import React, { useState, useEffect, useRef } from 'react'

import { toast } from 'react-toastify'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye } from '@fortawesome/free-solid-svg-icons'

import TablaDPP2_Extension from './tablas-vistas/TablaDPP2_Extension'
import { validarFecha, isDefaultDate } from '../../../helpers/fecha'
import { ApiUrl } from '../../../services/ApiRest'
import { axiosPostService } from '../../../services/asignacionLoteService/AsignacionLoteService'
import Modal from '../../../modales/shared/Modal'
import { useModal } from '../../../modales/shared/useModal'
import { ModalExtensionPermiso } from '../../../modales/extensionpermiso/ModalExtensionPermiso'

const DatosDPP2_Extension = ({ clientes, agencia }) => {

    let url = ""
    const permisoDesvioInicial = "DPP"
    const DPP = 'DPP'
    const Contado = 'Contado'
    const defaultDate = '1900-01-01'
    const ref = useRef();
    // const permisoDesvioExtension = "Contado"
    const [writtendata, setWrittendata] = useState({
        NumeroCliente     : clientes.length > 0 ? `${clientes[0].Num_cliente}`  : 0,
        PermisoDesvio     : permisoDesvioInicial,
        FolioDesvio       : "",
        FechaSolicitud    : "",
        FechaVencimiento  : "",
        FolioDPP2         : "",
        DocumentoPDF      : ""


    })
    const [cliente, setCliente] = useState(clientes.length > 0 ? `${clientes[0].Nombre_corto}` : "")
    const [isPreviewTable, setIsPreviewTable] = useState(false)
    const [VINClientes, setVINClientes] = useState([])
    const [VINClientesGenerados, setVINClientesGenerados] = useState([])
    const [foliosDesvioList, setFoliosDesvioList] = useState([])
    const [VINSGeneratedinBD, setVINSGeneratedinBD] = useState(false)
    const [table, setTable] = useState(permisoDesvioInicial)
    const [ isOpenModal, openModal, closeModal ] = useModal(false);
    const [extensionesPermisoByVIN, setExtensionesPermisoByVIN] = useState({VIN:"",FolioDesvio:""})


    useEffect(() => {
        if ( clientes.length > 0 ){
           setWrittendata({ 
            ...writtendata, 
            NumeroCliente : clientes[0].Num_cliente, 
          })
          setCliente(clientes[0].Nombre_corto)
          getFoliosDesvioByClienteAndPermiso( clientes[0].Num_cliente, writtendata.PermisoDesvio )
        }
    }, [clientes])

    const getFoliosDesvioByClienteAndPermiso = async ( ncliente, permisoDesvio ) => {
      url = ApiUrl + "api/dpp_contado/getfoliosdesviobycliente"
      const body_cliente = { Agencia: agencia, NumCliente: ncliente, permisoDesvio:permisoDesvio, pestana: "DPP2_Extensiones" };
      let total_folios_desvio = await axiosPostService( url, body_cliente )
      setFoliosDesvioList(total_folios_desvio)

      if ( total_folios_desvio.length > 0 ) {
        let firstFolioOfList = total_folios_desvio[0].FolioDesvio;
        getVinsByFolioDesvio( firstFolioOfList, ncliente, permisoDesvio )
        setWrittendata({ ...writtendata, NumeroCliente: ncliente, FolioDesvio: firstFolioOfList, PermisoDesvio: permisoDesvio })
      }

      if ( total_folios_desvio.length === 0 ) {
        setVINClientes([])
        if ( !isPreviewTable && VINClientesGenerados.length > 0 ) setVINClientesGenerados([]);
        setWrittendata({ ...writtendata, NumeroCliente: ncliente, FolioDesvio: "", PermisoDesvio: permisoDesvio })
        // toast.info("No se encontraron folios desvío del cliente y permiso desvío seleccionados.")
      }

    }

    const getVinsByFolioDesvio = async ( folioDesvio, ncliente, permisoDesvio ) => {
      url = ApiUrl + "api/dpp_contado/getvinsclientetoDPP2orExt";
      const body_cliente = { 
        Agencia       : agencia, 
        NumCliente    : ncliente, 
        permisoDesvio : permisoDesvio,
        folioDesvio   : folioDesvio 
      }

      if ( !isPreviewTable && VINClientesGenerados.length > 0 ) setVINClientesGenerados([]);
      let vins_by_folio_desvio = await axiosPostService( url, body_cliente )
      if ( vins_by_folio_desvio.length > 0 ) vins_by_folio_desvio = agregarVariableIsSelected( vins_by_folio_desvio );
      setVINClientes( vins_by_folio_desvio )

    }

    const agregarVariableIsSelected = ( vins_by_folio_desvio ) => {
      const list = vins_by_folio_desvio.map((obj) => {
        let addProperty = {
          ...obj,
          isVinSelected :  false,
        }
        return addProperty;
      })
      return list;
    }

    const OnChange = ( e ) => {

      if ( e.target.name === 'PermisoDesvio' ) {
          let permisodesv = e.target.value;
          setTable(permisodesv)
          getFoliosDesvioByClienteAndPermiso( writtendata.NumeroCliente, permisodesv )
          setVINSGeneratedinBD( false )
          return;
      }

      if ( e.target.name === 'FolioDesvio' ) {
        const [ foldesv, permdesv, numclient ] = e.target.value.split("|");
        getVinsByFolioDesvio( foldesv, writtendata.NumeroCliente, writtendata.PermisoDesvio )
        setWrittendata({ ...writtendata, FolioDesvio: foldesv })
        setVINSGeneratedinBD( false )
        return;
      }

      if ( e.target.name === 'DocumentoPDF' ) {
        setWrittendata({
          ...writtendata,
          [e.target.name] : e.target.files[0]
        })
        return;
      }

      setWrittendata({
        ...writtendata,
        [e.target.name] : e.target.value
      })
    }

    const changeSelectClientes = ( e ) => {
      const [ Ubicacion, Nombre_cliente, Num_cliente ] = e.target.value.split("|");
      getFoliosDesvioByClienteAndPermiso( Num_cliente, writtendata.PermisoDesvio )
      // setWrittendata({...writtendata, Cliente: Nombre_cliente})
      setCliente(Nombre_cliente);
      setVINSGeneratedinBD( false )
    }

    const handleVinSelected = ( e, registro ) => {
      const checked = e.target.checked;
      const updateVIN = VINClientes.map((row) => {
        if ( registro.VIN === row.VIN ) {
          let updateRow = {}

          if ( writtendata.PermisoDesvio ===  DPP ) {
            updateRow = {
              ...row,
              isVinSelected         : checked,
              FechaVencimientoFase2 : !checked ? defaultDate : writtendata.FechaVencimiento,
              FechaSolicitudFase2   : !checked ? defaultDate : writtendata.FechaSolicitud,
              FolioDPP2             : !checked ? ""          : writtendata.FolioDPP2
            }
          }

          if ( writtendata.PermisoDesvio ===  Contado ) {
            updateRow = {
              ...row,
              isVinSelected        : checked,
              FechaVencimientoExtP : !checked ? defaultDate : writtendata.FechaVencimiento,
              FechaSolicitudExtP   : !checked ? defaultDate : writtendata.FechaSolicitud,
              //Documento adjunto se enviará por separado
            }
          }
          insertNewVIN(updateRow, checked)
          return updateRow;
        }
  
        return row;
      })

      if ( VINSGeneratedinBD ) setVINSGeneratedinBD( false )
      setVINClientes(updateVIN)

    }

    const insertNewVIN = (object, checked) => {
      if ( !checked ) {
        const updateList = VINClientesGenerados.filter((row) => {
            return row.VIN !== object.VIN
        })
  
        setVINClientesGenerados(updateList)
        return;
      }

      setVINClientesGenerados([
        ...VINClientesGenerados,
        {
          ...object,
          Cliente : writtendata.NumeroCliente
        }
      ])
    }

    const showExtensions = (registro) => {
      setExtensionesPermisoByVIN({VIN: registro.VIN , FolioDesvio: registro.FolioDesvio})
      openModal();
    }

    const cerrarModalExt = () => {
      closeModal()
    }

    const onGenerateTable = () => {

      if ( existsSomeEmptyDate() !== undefined) {
        toast.info("Existen vins seleccionados sin fecha solicitud y vencimiento.")
        return;
      }

      if ( writtendata.PermisoDesvio === DPP && existsSomeEmptyFolioDPP2() !== undefined) {
        toast.info("Existen vins seleccionados sin folio DPP Fase 2.")
        return;
      }

      if ( VINSGeneratedinBD ) setVINClientesGenerados([]);
      setIsPreviewTable( !isPreviewTable )
    }

    const existsSomeEmptyFolioDPP2 = () => {
      return VINClientesGenerados.find( (obj) => {
        if ( obj.FolioDPP2 === "" ){
          return obj;
          }
      })
    }

    const existsSomeEmptyDate = () => {
      return VINClientesGenerados.find( (obj) => {
        if ( writtendata.PermisoDesvio === Contado ){
          if ( obj.FechaSolicitudExtP === "" || obj.FechaVencimientoExtP === "" ) return obj;
          return;
        }
        if ( obj.FechaSolicitudFase2 === "" || obj.FechaVencimientoFase2 === "" ) return obj;

      })
    }

    const afterRegister = () => {
      const foldes = writtendata.FolioDesvio;
      const numcli = writtendata.NumeroCliente;
      const permdes = writtendata.PermisoDesvio;
      setVINSGeneratedinBD( true )
      if ( writtendata.PermisoDesvio === Contado ) ref.current.value = "";
      getVinsByFolioDesvio( foldes, numcli, permdes );
    }

  return (
    <>
        <div className="row m-2">
          <Modal isOpen={isOpenModal} closeModal={closeModal}>
            <ModalExtensionPermiso 
              onSubmit={cerrarModalExt}
              extensionesPermisoByVIN={extensionesPermisoByVIN}
              agencia={agencia}
            />
          </Modal>
        <div className="col-6">
          <div className="row d-flex justify-content-between pl-2 pr-4">
              <h6 className='mr-4'>Seleccionar Cliente: </h6>
              <select 
              // name='Cliente' 
              className='form-select select-class-1' 
              onChange={(e) => changeSelectClientes(e)}
              tabIndex={1}
              disabled={isPreviewTable}
              >
                {
                clientes
                .map(cliente => {
                  return (
                    <option 
                    value={`${cliente.Ubicacion}|${cliente.Nombre_corto}|${cliente.Num_cliente}`} 
                    >
                        {`${cliente.Nombre_corto} ${cliente.Ubicacion}`}
                    </option>
                  )
                })
                }
              </select>
          </div>
        </div>
        <div className="col-6" >
          <div className="row d-flex justify-content-between pl-2 pr-4">
              <h6 className='mr-4'>Fecha Solicitud: </h6>
              <input 
              className='input-class' 
              type="date" 
              name="FechaSolicitud" 
              value={writtendata.FechaSolicitud} 
              onChange={OnChange}
              disabled={isPreviewTable}
              tabIndex={5}
              min="2022-01-01"
              />
          </div>
        </div>
        
      </div>
      <div className="row m-2">
      <div className="col-6">
          <div className="row d-flex justify-content-between pl-2 pr-4">
              <h6 className='mr-4'>Permiso Desvío: </h6>
              <select 
              name='PermisoDesvio' 
              className='form-select select-class-1' 
              tabIndex={2} 
              onChange={OnChange}
              disabled={isPreviewTable}
              >
                    <option value="DPP"> DPP Fase 1 a DPP Fase 2 </option>
                    <option value="Contado"> Contado a Extensión Permiso </option>
              </select>
          </div>
        </div>
        
        <div className="col-6" >
          <div className="row d-flex justify-content-between pl-2 pr-4">
              <h6 className='mr-4'>Fecha Vencimiento: </h6>
              <input 
              className='input-class' 
              type="date" 
              name="FechaVencimiento" 
              value={writtendata.FechaVencimiento} 
              onChange={OnChange}
              disabled={isPreviewTable}
              tabIndex={6}
              min="2022-01-01"
              />
          </div>
        </div>
      </div>
      <div className="row m-2">
        <div className="col-6" >
          <div className="row d-flex justify-content-between pl-2 pr-4">
              <h6 className='mr-4'>Folio Desvío: </h6>
              <select 
              name='FolioDesvio' 
              className='form-select select-class-1' 
              tabIndex={2} 
              onChange={OnChange} 
              disabled={ foliosDesvioList.length === 0 || isPreviewTable}
              >
                    {
                      foliosDesvioList.map(foliod => {
                        return (
                          <option value={ `${foliod.FolioDesvio}|${foliod.PermisoDesvio}|${foliod.Cliente}` }>
                              { foliod.FolioDesvio }
                          </option>
                        )
                      })
                    }
              </select>
          </div>
        </div>
        {
          writtendata.PermisoDesvio === DPP ?
          <div className="col-6" >
          <div className="row d-flex justify-content-between pl-2 pr-4">
              <h6 className='mr-4'>Folio DPP Fase 2: </h6>
              <input 
              className='input-class' 
              type="text" 
              name="FolioDPP2" 
              value={writtendata.FolioDPP2} 
              onChange={OnChange}
              tabIndex={8}
              disabled={isPreviewTable}
              />
          </div>
        </div>
          :
        <div className="col-6" >
          <div className="row d-flex">
              <h6 className='ml-2'>Documento PDF: </h6>
              <input 
              className='custom-file-upload'
              style={{ border:'none' }}
              type="file" 
              name="DocumentoPDF"
              accept='.pdf'               
              onChange={OnChange}
              tabIndex={8}
              disabled={isPreviewTable}
              ref={ref}
              />
          </div>
        </div>
        }
      </div>


      <div className="row m-2 d-flex justify-content-between">
        { writtendata.PermisoDesvio === DPP 
        ? <h6 className='ml-2'>Seleccionar VIN's que integran el DPP Fase 2</h6>
        : <h6 className='ml-2'>Seleccionar VIN's que integran la Extensión Permiso</h6>}
        <button 
        type='button' 
        className='btn btn-info mt-2 mb-2' 
        onClick={onGenerateTable}
        disabled={VINClientesGenerados.length === 0}
        >
          { isPreviewTable ? 'Regresar' : writtendata.PermisoDesvio === DPP ? <small>Aplicar DPP Fase 2</small> : <small>Aplicar Extensión Permiso</small>}
        </button>
      </div>
      {
        !isPreviewTable && 
        table !== "DPP"
        ? 
        <div className="row m-2">
          <table className='table display compact'>
            <thead style={{backgroundColor:'#1565C0', color:'white'}}>
              <tr className='text-center'>
                <th>VIN</th>
                <th>Seleccionar</th>
                <th>Permiso Desvío</th>
                <th>Folio Desvío</th>
                <th>Fecha Vencimiento</th>
                <th>Fecha Solicitud Ext.</th>
                <th>Fecha Vencimiento Ext.</th>
                <th>Mostrar Extensiones</th>
              </tr>
            </thead>
            <tbody style={{backgroundColor:'#FFFFE0'}}>{/* #FFFACD */}
            {
              VINClientes.length > 0 && table === Contado
              ? VINClientes.map((registro) => {
                return (
                  <tr className='text-center'>
                    <td>{registro.VIN}</td>
                    <td>
                      <input 
                      checked={registro.isVinSelected} 
                      className="form-check-input" 
                      name="vin_selected" 
                      onChange={( e ) => handleVinSelected( e, registro )}
                      type="checkbox" 
                      // disabled={ true } 
                      />
                    </td>
                    <td>{registro.PermisoDesvio}</td>
                    <td>{registro.FolioDesvio}</td>
                    <td>{validarFecha(registro.FechaVencimiento)}</td>
                    <td>{isDefaultDate(registro.FechaSolicitudExtP)}</td>
                    <td>{isDefaultDate(registro.FechaVencimientoExtP)}</td>
                    <td>
                      <button
                       title='Ver las fechas de las extensiones'
                       type='button'
                       className='btn btn-outline-dark'
                       onClick={() => showExtensions(registro)}
                       >
                         <FontAwesomeIcon icon={faEye}/>
                      </button>
                    </td>
                    {/* <td>{registro.DocumentoAdjunto}</td> */}
                  </tr>
                )
              })
              :
              <tr className='p-2'>
                  <td>No existen registros para el cliente seleccionado.</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
              </tr>
            }
            </tbody>
          </table>
      </div>
      :
      !isPreviewTable && 
      <div className="row m-2">
          <table className='table display compact'>
            <thead style={{backgroundColor:'#1565C0', color:'white'}}>
              <tr className='text-center'>
                <th>VIN</th>
                <th>Seleccionar</th>
                <th>Permiso Desvío</th>
                <th>Folio DPP</th>
                <th>Fecha Vencimiento</th>
                <th>Fecha Vencimiento DPP 1</th>
                <th>Fecha Solicitud DPP 2</th>
                <th>Fecha Vencimiento DPP 2</th>
                <th>Folio DPP 2</th>
              </tr>
            </thead>
            <tbody style={{backgroundColor:'#FFFFE0'}}>{/* #FFFACD */}

            { 
            VINClientes.length > 0 && table === DPP
              ?
              VINClientes
              .map((registro) => {
                return (
                  <tr className='text-center'>
                    <td>{registro.VIN}</td>
                    <td>
                    <input 
                      checked={registro.isVinSelected}
                      className="form-check-input" 
                      name="vin_selected" 
                      onChange={( e ) => handleVinSelected( e, registro )}
                      type="checkbox" 
                      // disabled={ true } 
                      />
                    </td>
                    <td>{registro.PermisoDesvio}</td>
                    <td>{registro.FolioDPP}</td>
                    <td>{validarFecha(registro.FechaVencimiento)}</td>
                    <td>{validarFecha(isDefaultDate(registro.FechaVencimientoDPP1))}</td>
                    <td>{validarFecha(isDefaultDate(registro.FechaSolicitudFase2))}</td>
                    <td>{validarFecha(isDefaultDate(registro.FechaVencimientoFase2))}</td>
                    <td>{registro.FolioDPP2}</td>
                  </tr>
                )
              })
              :
              <tr className='p-2'>
                  <td>No existen registros para el cliente seleccionado.</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
              </tr> 
            } 
            </tbody>
          </table>
      </div>
      }

      <TablaDPP2_Extension
        agencia={agencia}
        data={VINClientesGenerados}
        isPreviewTable={isPreviewTable}
        valorPermisoDesvio={writtendata.PermisoDesvio}
        documentoPDF={writtendata.DocumentoPDF}
        Cliente={cliente}
        afterRegister={afterRegister}
      /> 
     
      <div className="row m-2">
        <button 
        type='button' 
        className='btn btn-info mt-2 mb-2' 
        onClick={onGenerateTable} 
        disabled={VINClientesGenerados.length === 0} 
        >
          { isPreviewTable ? 'Regresar' : writtendata.PermisoDesvio === DPP ? <small>Aplicar DPP Fase 2</small> : <small>Aplicar Extensión Permiso</small>}
        </button>
      </div>
    </>
  )
}

export default DatosDPP2_Extension